import { Injectable, NestMiddleware } from "@nestjs/common";
import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class JwtExtractionMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction) {
        const authHeader = req.headers.authorization;
        if (authHeader && authHeader.startsWith('Bearer ')) {
            const token = authHeader.substring(7, authHeader.length);
            try {
                const decoded = jwt.decode(token);
                req['userId'] = decoded['sub'];
            } catch (error) {
                console.error('JWT decoding failed:', error);
            }
        }
        next();
    }
}