import { Exclude } from "class-transformer";
import { IsBoolean, IsNotEmpty, IsString, Length, ValidateIf } from "class-validator";
import { UpdateDateColumn } from "typeorm";

export class UpdateCategoryDto {
    @ValidateIf((obj) => obj.name !== undefined)
    @IsString()
    @Length(4, 30)
    readonly name?: string;

    @ValidateIf((obj) => obj.isActive !== undefined)
    @IsBoolean()
    @IsNotEmpty()
    readonly isActive?: boolean;

    @Exclude()
    slug?: string;

    @UpdateDateColumn()
    readonly updatedAt?: Date;
}