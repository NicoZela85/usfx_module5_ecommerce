import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './category.entity';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@Injectable()
export class CategoriesService {
  constructor(@InjectRepository(Category) private categoryRepository: Repository<Category>) {}

  async getCategories() {
    return await this.categoryRepository.find();
  }

  getCategory(id: number) {
    return this.categoryRepository.findOne({
      where: { id }, relations: { products: true },
    });
  }

  createCategory(category: CreateCategoryDto) {
    const newCategory = this.categoryRepository.create(category);
    return this.categoryRepository.save(newCategory);
  }

  async updateNameCategory(id: number, category: UpdateCategoryDto) {
    const existingCategory =  await this.categoryRepository.findOne({ where: {id}});
    const { name, slug } = category;
    const categoryUpdateData = { name, slug };
    existingCategory.updatedAt = new Date();

    Object.assign(existingCategory, categoryUpdateData);

    return this.categoryRepository.save(existingCategory); 
  }
  
  async updateIsActiveCategory(id: number, category: UpdateCategoryDto) {
    const existingCategory =  await this.categoryRepository.findOne({ where: {id}});
    const { isActive } = category;
    const categoryUpdateData = { isActive };
    existingCategory.updatedAt = new Date();

    Object.assign(existingCategory, categoryUpdateData);

    return this.categoryRepository.save(existingCategory); 
  }

  async deleteCategory(id: number) {
    const deletedCategory = await this.categoryRepository.delete(id);
    return deletedCategory ? true : false;
  }

  async existsCategory(slug: string): Promise<boolean> {
    const existsCategory = await this.categoryRepository.findOne({ where: { slug } });
    return existsCategory ? true : false;
  }

  async thereAreProductsAssociatedWithCategory(id: number): Promise<boolean> {
    const category = await this.categoryRepository.findOne({
      where: { id }, relations: { products: true },
    });

    return category.products.length > 0;
  }
}