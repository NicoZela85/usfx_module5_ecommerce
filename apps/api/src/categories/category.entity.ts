import { BaseEntity } from "src/common/base.entity";
import { Product } from "src/products/product.entity";
import { Column, Entity, OneToMany } from "typeorm";

@Entity({ name: 'categories' })
export class Category extends BaseEntity {
    @Column({ length: 30, unique: true })
    name: string;

    @Column({ length: 40, unique: true })
    slug: string;

    @OneToMany(() => Product, product => product.category)
    products: Product[];
}