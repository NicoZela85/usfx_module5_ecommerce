import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Roles } from 'nest-keycloak-connect';
import { stripHtmlTagsAndExtraSpaces } from 'src/utils/inputUtils';
import { toSlug } from 'src/utils/stringUtils';
import { handleCategoryErrorWithProducts, handleDuplicateCategoryError, handleNonExistingCategoryError, handleSuccessfulDeletionResponse } from 'src/common/http-handling-category';
import { CategoryOneResource } from 'src/categories/resources/category-one.resource';
import { CategoryCollection } from 'src/categories/resources/category.collection';
import { CategoryResource } from 'src/categories/resources/category.resource';

@Controller('/api/v1/categories')
export class CategoriesController {
    constructor(private categoriesService: CategoriesService) {}

    @Roles({roles:['admin', 'customer']})
    @Get()
    async getAll() {
        const categories = await this.categoriesService.getCategories()
        return new CategoryCollection(categories).format();
    }

    @Roles({roles:['admin', 'customer']})
    @Get('/:id')
    async getOne(@Param('id', ParseIntPipe) id: number) {
        const category = await this.categoriesService.getCategory(id);
        if (!category) {
            return handleNonExistingCategoryError();
        }

        return new CategoryResource(category).format();
    }

    @Roles({roles:['admin']})
    @Post()
    async create(@Body() newCategory: CreateCategoryDto) {
        const name = stripHtmlTagsAndExtraSpaces(newCategory.name);
        const slug = toSlug(name);

        if (await this.categoriesService.existsCategory(slug)) {
            return handleDuplicateCategoryError();
        }

        const categoryToCreate = { ...newCategory, name, slug };
        const category = await this.categoriesService.createCategory(categoryToCreate);
        return new CategoryOneResource(category).format();
    }

    @Roles({roles:['admin']})
    @Patch('/:id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() category: UpdateCategoryDto) {
        const existingCategory = await this.categoriesService.getCategory(id);
        if (!existingCategory) {
            return handleNonExistingCategoryError();
        }

        if (category.name !== undefined) {
            const name = stripHtmlTagsAndExtraSpaces(category.name);
            const slug = toSlug(name);

            if (existingCategory.slug === slug) {
                return new CategoryOneResource(existingCategory).format();
            }

            if (await this.categoriesService.existsCategory(slug)) {
                return handleDuplicateCategoryError();
            }

            const categoryToUpdate = { ...category, name, slug };
            const response = await this.categoriesService.updateNameCategory(id, categoryToUpdate);
            return new CategoryOneResource(response).format();
        } 
        
        if (category.isActive !== undefined) {
            const categoryToUpdate = { ...category, isActive: category.isActive};
            const response = await this.categoriesService.updateIsActiveCategory(id, categoryToUpdate);
            return new CategoryOneResource(response).format();
        }
    }

     @Roles({roles:['admin']})
    @Delete('/:id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        if (!await this.categoriesService.getCategory(id)) {
            return handleNonExistingCategoryError();
        }

        if (await this.categoriesService.thereAreProductsAssociatedWithCategory(id)) {
            return handleCategoryErrorWithProducts();
        }

        const result = await this.categoriesService.deleteCategory(id);
        if (!result) {
            return handleNonExistingCategoryError();
        }
        return handleSuccessfulDeletionResponse();
    }
}