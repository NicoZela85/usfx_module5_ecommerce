import { Category } from "src/categories/category.entity";

export class CategoryCollection {
    constructor(private categories: Category[]) {}
  
    format()  {
        return {
            type: 'categories',
            meta: {
                totalItems: this.categories.length,
            },
            data: this.categories,
        };
    }
}
  