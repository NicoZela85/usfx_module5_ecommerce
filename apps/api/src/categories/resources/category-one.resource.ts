import { Category } from "src/categories/category.entity";

export class CategoryOneResource {
    constructor(private category: Category) {}
  
    format()  {
        return {
            data: {
                type: 'categories',
                id: this.category.id,
                attributes: {
                    name: this.category.name,
                    slug: this.category.slug,
                    isActive: this.category.isActive,
                    createdAt: this.category.createdAt,
                    updatedAt: this.category.updatedAt,
                },
            }
        };
    }
}
  