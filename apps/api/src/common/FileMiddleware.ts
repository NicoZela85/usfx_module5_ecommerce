import { NestMiddleware, Injectable, HttpException, HttpStatus } from '@nestjs/common';

@Injectable()
export class FileUploadErrorMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    if (req.file && req.file.size > 7 * 1024 * 1024) {
      req.fileValidationError = 'File too large';
    }
    next();
  }
}
