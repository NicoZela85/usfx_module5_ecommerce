import { HttpException, HttpStatus } from "@nestjs/common";

export function handleUserNotFound() {
    throw new HttpException(
        {
        message: ['User not found'],
        error: 'Not Found',
        statusCode: HttpStatus.NOT_FOUND,
    }, HttpStatus.NOT_FOUND);
}