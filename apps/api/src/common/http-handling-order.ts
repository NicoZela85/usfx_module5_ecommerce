import { HttpException, HttpStatus } from "@nestjs/common";

export function handleNotAuthorized() {
    throw new HttpException(
        {
        message: ["Not authorized"],
        error: "Unauthorized",
        statusCode: HttpStatus.UNAUTHORIZED,
        },
        HttpStatus.UNAUTHORIZED
    );
}

export function handleNotExistOrderError() {
  throw new HttpException(
    {
      message: ["No order found"],
      error: "Not Found",
      statusCode: HttpStatus.NOT_FOUND,
    },
    HttpStatus.NOT_FOUND
  );
}

export function handleOrderCompletedError() {
    throw new HttpException(
        {
        message: ["The order has already been completed"],
        error: "Bad Request",
        statusCode: HttpStatus.BAD_REQUEST,
        },
        HttpStatus.BAD_REQUEST
    );
}

export function handleOrderCancelledError() {
  throw new HttpException(
    {
      message: ["The order has already been cancelled"],
      error: "Bad Request",
      statusCode: HttpStatus.BAD_REQUEST,
    },
    HttpStatus.BAD_REQUEST
  );
}

export function handleShoppingCartIsEmptyError() {
  throw new HttpException(
    {
      message: ["The shopping cart is empty"],
      error: "Bad Request",
      statusCode: HttpStatus.BAD_REQUEST,
    },
    HttpStatus.BAD_REQUEST
  );
}

export function handleNotExistingProductError(id: number) {
  throw new HttpException(
    {
        message: [`No product found with ID ${id}`],
        error: "Not Found",
        statusCode: HttpStatus.NOT_FOUND,
    },
    HttpStatus.NOT_FOUND
  );
}

export function handleUnavailableStockOfProductError(id: number) {
  throw new HttpException(
    {
        message: [`The product with ID ${id} is out of stock`],
        error: "Bad Request",
        statusCode: HttpStatus.BAD_REQUEST,
    },
    HttpStatus.BAD_REQUEST
  );
}

export function handleStockNotEnoughError(id: number) {
  throw new HttpException(
    {
        message: [`The product with ID ${id} does not have enough stock`],
        error: "Bad Request",
        statusCode: HttpStatus.BAD_REQUEST,
    },
    HttpStatus.BAD_REQUEST
  );
}




