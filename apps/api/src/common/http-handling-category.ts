import { HttpException, HttpStatus } from '@nestjs/common';

export function handleDuplicateCategoryError() {
  throw new HttpException(
    {
      message: ['There is already a category with this name'],
      error: 'Bad Request',
      statusCode: HttpStatus.BAD_REQUEST,
    },
    HttpStatus.BAD_REQUEST,
  );
}

export function handleNonExistingCategoryError() {
  throw new HttpException(
    {
      message: ['No category found with that ID'],
      error: 'Not Found',
      statusCode: HttpStatus.NOT_FOUND,
    },
    HttpStatus.NOT_FOUND,
  );
}

export function handleCategoryErrorWithProducts() {
  throw new HttpException(
    {
      message: ['The category cannot be deleted since it has associated products'],
      error: 'Bad Request',
      statusCode: HttpStatus.CONFLICT,
    },
    HttpStatus.CONFLICT,
  );
}

export function handleSuccessfulDeletionResponse() {
  throw new HttpException(
    {
      message: ['The category was successfully deleted'],
      statusCode: HttpStatus.OK,
    },
    HttpStatus.OK,
  );
}