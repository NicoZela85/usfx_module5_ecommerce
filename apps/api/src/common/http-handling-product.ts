import { HttpException, HttpStatus } from '@nestjs/common';

export function handleDuplicateProductError() {
  throw new HttpException(
    {
      message: ['There is already a product with this name'],
      error: 'Bad Request',
      statusCode: HttpStatus.BAD_REQUEST,
    },
    HttpStatus.BAD_REQUEST,
  );
}

export function handleNonExistingProducError() {
  throw new HttpException(
    {
      message: ['No product found with that ID'],
      error: 'Not Found',
      statusCode: HttpStatus.NOT_FOUND,
    },
    HttpStatus.NOT_FOUND,
  );
}

export function handleNonExistingProducSlugError() {
  throw new HttpException(
    {
      message: ['No product found with that Slug'],
      error: 'Not Found',
      statusCode: HttpStatus.NOT_FOUND,
    },
    HttpStatus.NOT_FOUND,
  );
}

export function handleSuccessfulDeletionResponse() {
  throw new HttpException(
    {
      message: ['The product was successfully deleted'],
      statusCode: HttpStatus.OK,
    },
    HttpStatus.OK,
  );
}

export function handleInvalidFileTypeError() {
  throw new HttpException(
    {
      message: ['Invalid file type. Only jpg, jpeg, and png files are allowed'],
      error: 'Bad Request',
      statusCode: HttpStatus.BAD_REQUEST,
    },
    HttpStatus.BAD_REQUEST,
  );
}

export function handleFileTooLargeError() {
  throw new HttpException(
    {
      message: ['File is too large. Maximum file size is 7MB'],
      error: 'Bad Request',
      statusCode: HttpStatus.BAD_REQUEST,
    },
    HttpStatus.BAD_REQUEST,
  );
}
