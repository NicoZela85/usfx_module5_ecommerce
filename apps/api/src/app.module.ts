import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import {AuthGuard, KeycloakConnectModule, ResourceGuard, RoleGuard} from 'nest-keycloak-connect';
import { AppController } from './app.controller';
import { KeycloakConfigModule } from './config/keycloak-config.module';
import { KeycloakConfigService } from './config/keycloak-config.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoriesModule } from './categories/categories.module';
import { ProductsModule } from './products/products.module';
import { TypeOrmConfigModule } from './database/typeorm.module';
import { ShoppingCartModule } from './shopping-cart/shopping-cart.module';
import { JwtExtractionMiddleware } from './jwt-extraction.middleware';
import { OrdersModule } from './orders/orders.module';

@Module({
  imports: [
    KeycloakConnectModule.registerAsync({
      useExisting: KeycloakConfigService,
      imports: [KeycloakConfigModule]
    }),
    TypeOrmConfigModule,
    CategoriesModule,
    ProductsModule,
    ShoppingCartModule,
    OrdersModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: ResourceGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RoleGuard,
    },
  ],
  controllers: [AppController],
})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
      consumer
      .apply(JwtExtractionMiddleware)
      .forRoutes('*');
  }
}