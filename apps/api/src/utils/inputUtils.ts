export function stripHtmlTagsAndExtraSpaces(input: string): string {
  const withoutHtmlTags = input.replace(/<[^>]*>/g, '');
  return withoutHtmlTags.replace(/\s+/g, ' ').trim();
}