import { Controller, Get, Post, Body, Patch, Param, Delete, Req, Put } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';

import { Roles } from 'nest-keycloak-connect';

import { UpdateOrderStatusDto } from './dto/update-order-status.dto';
import { ShoppingCartService } from 'src/shopping-cart/shopping-cart.service';
import { handleUserNotFound } from 'src/common/http-handling-user';
import { handleNotAuthorized, handleNotExistOrderError, handleOrderCancelledError, handleOrderCompletedError, handleShoppingCartIsEmptyError } from 'src/common/http-handling-order';
import { OrderCollection } from './resources/order.collection';
import { OrderResource } from './resources/order.resource';
import { OrderStatus } from './enums/order-status.enum';
import { OrderOneResource } from './resources/order-one.resource';
import { OrdersService } from './orders.service';
import { OrderPaymentStatus } from './enums/order-payment-status.enum';

@Controller('/api/v1/order')
export class OrdersController {
  constructor(
    private readonly orderService: OrdersService,
    private readonly shoppingCartService: ShoppingCartService
  ) {}

  @Roles({ roles: ['customer']})
  @Post()
  async create(@Body() createOrderDto: CreateOrderDto, @Req() req){
    const userId = req.userId;
    if (!userId) {
      handleUserNotFound();
    }
    if (await this.orderService.verifyShoppingCartIsEmpty(userId) === true) {
      return handleShoppingCartIsEmptyError();
    }
    try {
      this.orderService.verifyItemsProdcuts(await this.shoppingCartService.getCartItems(userId));
    }catch (error) {
      throw new Error(error.message);
    }

    const order = await this.orderService.createOrder(createOrderDto, userId);
    return new OrderOneResource(order).format();
  }

  @Roles({ roles: ['admin', 'customer']})
  @Put('/:id')
  async updateOrderStatus(@Param('id') id: number, @Req() req, @Body() updateOrderStatusDto: UpdateOrderStatusDto) {    
    const { status, statusPayment} = updateOrderStatusDto;
    
    const userId = req.userId;
    if (!userId) {
      return handleUserNotFound();
    }
    let order;
    
    //let role = req.user['resource_access']['nest-backend']['roles'][0];
    let role = req.user?.resource_access?.['nest-backend']?.roles?.[0];
    if (role === 'admin') {
      //consult if there are orders
      
      order = await this.orderService.getOrderById(id);
    }else if (role === 'customer') {
      //consult if the user has an order
      order = await this.orderService.getOrdersByUserId(+id, userId);
    }
    if (!order) {
      return handleNotExistOrderError();
    }
    if ((order.status === OrderStatus.COMPLETED) && (order.statusPayment === OrderPaymentStatus.PAID)){
      return handleOrderCompletedError();
    }
    if ((order.status === OrderStatus.CANCELLED)){
      return handleOrderCancelledError();
    }
    
    if (status === OrderStatus.COMPLETED && role === 'customer'){
      return handleNotAuthorized();
    }
    
    const orderUpdated = await this.orderService.updateOrderStatus(role, status, statusPayment, order)
    if (!orderUpdated) {
      return handleNotExistOrderError();
    }
    return new OrderResource(orderUpdated).format();
  }

  @Roles({ roles: ['admin']})
  @Get('/all')
  async getAllOrders() {
    const orders = await this.orderService.getAllOrders();
    return new OrderCollection(orders).format();
  }

  @Roles({ roles: ['customer']})
  @Get('/:id')
  async getOrdersByUserId(@Param('id') id: number, @Req() req) {
    const userId = req.userId;
    if (!userId) {
      return handleUserNotFound();
    }
    const orderItems = await this.orderService.getOrdersByUserId(+id, userId);
    return new OrderResource(orderItems).format();
  }

  
}
