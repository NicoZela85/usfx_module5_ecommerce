import { IsEmpty, IsEnum, IsNotEmpty, IsString, IsUUID } from "class-validator";
import { OrderStatus } from "../enums/order-status.enum";

export class CreateOrderDto {
    
    @IsString()
    @IsNotEmpty()
    address: string;

    @IsNotEmpty()
    @IsString()
    contact: string;

}
