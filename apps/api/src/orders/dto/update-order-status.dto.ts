import { IsEnum, IsNotEmpty } from "class-validator";
import { OrderStatus } from "../enums/order-status.enum";
import { OrderPaymentStatus } from "../enums/order-payment-status.enum";

export class UpdateOrderStatusDto {
    @IsNotEmpty()
    @IsEnum(OrderStatus)
    status: OrderStatus;

    @IsEnum(OrderPaymentStatus)
    statusPayment?: OrderPaymentStatus = OrderPaymentStatus.PENDING;
}