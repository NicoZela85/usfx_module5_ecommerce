export enum OrderPaymentStatus {
    PENDING = 'PENDING',
    PAID = 'PAID'
}