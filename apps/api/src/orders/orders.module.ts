import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/orderItem.entity';
import { Product } from 'src/products/product.entity';
import { ShoppingCartService } from 'src/shopping-cart/shopping-cart.service';
import { ShoppingCart } from 'src/shopping-cart/entities/shopping-cart.entity';
import { ShoppingCartItem } from 'src/shopping-cart/entities/shopping-cart-item.entity';
import { ProductsService } from 'src/products/products.service';
import { CategoriesService } from 'src/categories/categories.service';
import { Category } from 'src/categories/category.entity';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderItem, Product, ShoppingCart, ShoppingCartItem, Product, Category])],
  controllers: [OrdersController],
  providers: [OrdersService, ShoppingCartService, ProductsService, CategoriesService],
})
export class OrdersModule {}
