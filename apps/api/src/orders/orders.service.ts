import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/orderItem.entity';
import { In, Repository } from 'typeorm';
import { ShoppingCartService } from 'src/shopping-cart/shopping-cart.service';
import { ShoppingCartItem } from 'src/shopping-cart/entities/shopping-cart-item.entity';
import { OrderStatus } from './enums/order-status.enum';
import { ProductsService } from 'src/products/products.service';
import { Product } from 'src/products/product.entity';
import { handleNotAuthorized, handleNotExistingProductError, handleStockNotEnoughError } from 'src/common/http-handling-order';
import { OrderPaymentStatus } from './enums/order-payment-status.enum';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private readonly orderItemRepository: Repository<OrderItem>,
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    
    private readonly shoppingCartService: ShoppingCartService,
    private readonly productsService: ProductsService,


  ) {}

  async createOrder(createOrderDto: CreateOrderDto, userId: string): Promise<Order> {
    const {address, contact} = createOrderDto;

    const orderTotal: number = await this.shoppingCartService.totalCartPrice(userId);

    const order: Order = this.orderRepository.create({
      userId: userId,
      address: address,
      contact: contact,
      total: orderTotal
    });

    await this.orderRepository.save(order);

    this.addItemsToOrder(order, await this.shoppingCartService.getCartItems(userId));
    // Update product stock
    const cartItems = await this.shoppingCartService.getCartItems(userId);
    await this.updateStockInProgress(cartItems);

    // Clear cart
    await this.shoppingCartService.clearCart(userId);

    return order;

  }

  async getAllOrders(): Promise<Order[]> {
    const orders = await this.orderRepository.find();
    if (!orders) {
      throw new Error('No orders found');
    }
    return orders;
  }

  async getOrdersByUserId(id: number, userId: string){
    const order = await this.orderRepository.findOne({ where: { id, userId }, relations: {items: true}});
    return order;
  }

  async getOrderById(id: number) {
    const order = await this.orderRepository.findOne({ where: { id } });
    return order;
  }


  async updateOrderStatus(role: string, status: OrderStatus, statusPayment: OrderPaymentStatus, order: Order) {
    
    if (status === OrderStatus.CANCELLED) {
      await this.updateStockInCanceled(await this.shoppingCartService.getCartItems(order.userId));
    }
    order.status = status;
    if(role === 'admin' && (status === OrderStatus.COMPLETED || order.status === OrderStatus.COMPLETED)){
      order.statusPayment = statusPayment;
    }
    
    return await this.orderRepository.save(order);
  }

  private async addItemsToOrder(order: Order, cartItems: ShoppingCartItem[]): Promise<OrderItem[]> {
    const orderItems = [];
    for (const cartItem of cartItems) {
      const {product, quantity, totalPrice} = cartItem;
      const orderItem = this.orderItemRepository.create({
        order,
        product: product.name,
        quantity,
        totalPrice,
      });
      orderItems.push(orderItem);
    }
    return this.orderItemRepository.save(orderItems);
  }

  async verifyShoppingCartIsEmpty(userId: string): Promise<boolean>{
    const cartItems = await this.shoppingCartService.getCartItems(userId);
    if (cartItems.length === 0) {
      return true;
    }
    return false;
  }

  async verifyItemsProdcuts(cartItems: ShoppingCartItem[]) {
    for (const cartItem of cartItems) {
      return await this.verifyProductAndStock(cartItem.product.id, cartItem.quantity);
    }
  }

  private async verifyProductAndStock(productId: number, quantity: number){
    const isValidProduct = await this.isVaildProduct(productId);
    if (!isValidProduct) {
      return handleNotExistingProductError(productId);
    }
    const isVailableStock = await this.isVailableStock(productId);
    if (!isVailableStock) {
      return handleStockNotEnoughError(productId);
    }
    const isStockEnough = await this.isStockEnough(productId, quantity);
    if (!isStockEnough) {
      return handleStockNotEnoughError(productId);
    }
  }

  private async isVaildProduct(productId: number): Promise<boolean> {
    const product = await this.productRepository.findOne({ where: { id: productId } });
    if (!product) {
      return false;
    }
    return true;
  }

  private async isVailableStock(productId: number): Promise<boolean> {
    const product = await this.productRepository.findOne({ where: { id: productId } });
    if (product.stock === 0) {
      return false;
    }
    return true;
  }

  private async isStockEnough(productId: number, quantity: number): Promise<boolean> {
    const product = await this.productRepository.findOne({ where: { id: productId } });
    if (product.stock < quantity) {
      return false;
    }
    return true;
  }

  private async updateStockInProgress(cartItems: ShoppingCartItem[]) {
    for (const cartItem of cartItems) {
      const product = cartItem.product;
      product.stock -= cartItem.quantity;
      await this.productsService.updateProduct(product.id, product);
    }
  }

  private async updateStockInCanceled(cartItems: ShoppingCartItem[]) {
    for (const cartItem of cartItems) {
      const product = cartItem.product;
      product.stock += cartItem.quantity;
      await this.productsService.updateProduct(product.id, product);
    }
  }
}
