import { Order } from "../entities/order.entity";

export class OrderCollection {
    constructor(private orders: Order[]) {}
  
    format()  {
        return {
            type: 'orders',
            meta: {
                totalItems: this.orders.length,
            },
            data: this.orders,
        };
    }
}