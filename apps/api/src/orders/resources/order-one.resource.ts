import { Order } from "../entities/order.entity";

export class OrderOneResource {
    constructor(private order: Order) {}

    format()  {
        return {
            data: {
                type: 'orders',
                id: this.order.id,
                attributes: {
                    userId: this.order.userId,
                    address: this.order.address,
                    contact: this.order.contact,
                    statusPayment: this.order.statusPayment,
                    total: this.order.total,
                    status: this.order.status,
                    createdAt: this.order.createdAt,
                },
            }
        };
    }
}