import { Column, Entity, JoinColumn, ManyToOne, OneToMany, Or, PrimaryGeneratedColumn } from "typeorm";
import { OrderStatus } from "../enums/order-status.enum";
import { OrderItem } from "./orderItem.entity";
import { OrderPaymentStatus } from "../enums/order-payment-status.enum";

@Entity()
export class Order {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column('uuid')
    userId: string;
    
    @Column()
    address: string;

    @Column()
    contact: string;
    
    @Column({default: OrderPaymentStatus.PENDING})
    statusPayment: string;

    @Column( {type: 'decimal', precision: 10, scale: 2})
    total: number;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    createdAt: Date;
    
    @OneToMany(() => OrderItem, item => item.order)
    items: OrderItem[];

    @Column({default: OrderStatus.PENDING})
    status: OrderStatus;

}
