import { Injectable } from '@nestjs/common';
import { KeycloakConnectOptions, KeycloakConnectOptionsFactory, TokenValidation } from 'nest-keycloak-connect';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class KeycloakConfigService implements KeycloakConnectOptionsFactory {
  constructor(private readonly configService: ConfigService){}
  createKeycloakConnectOptions(): KeycloakConnectOptions {
    return {
      authServerUrl: this.configService.get<string>('AUTH_SERVER_URL'),
      realm: this.configService.get<string>('REALM'),
      clientId: this.configService.get<string>('BACKEND_CLIENT_ID'),
      secret: this.configService.get<string>('BACKEND_SECRET'),
      logLevels: ['verbose'],
      useNestLogger: false,
      tokenValidation: TokenValidation.ONLINE,
    };
  }
}