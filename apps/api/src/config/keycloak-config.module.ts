import { Module } from "@nestjs/common";
import { KeycloakConfigService } from "./keycloak-config.service";
import { ConfigModule } from "@nestjs/config";

@Module({
    imports: [ConfigModule.forRoot()],
    providers: [KeycloakConfigService],
    exports: [KeycloakConfigService],
})
export class KeycloakConfigModule{}