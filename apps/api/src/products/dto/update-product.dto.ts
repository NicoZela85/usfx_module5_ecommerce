import { Exclude, Transform } from "class-transformer";
import { Contains, IsBoolean, IsInt, IsNotEmpty, IsNumber, IsPositive, IsString, Length, Min, ValidateIf } from "class-validator";
import { UpdateDateColumn } from "typeorm";

export class UpdateProductDto{
    @ValidateIf((obj) => obj.name !== undefined)
    @IsString()
    @Length(4, 150)
    readonly name?: string;

    @Exclude()
    slug: string;

    @ValidateIf((obj) => obj.description !== undefined)
    @IsString()
    @IsNotEmpty()
    readonly description?: string;

    @ValidateIf((obj) => obj.price !== undefined)
    @Transform(({ value }) => Number(value))
    @IsNumber()
    @IsNotEmpty()
    @IsPositive()
    readonly price?: number;

    @ValidateIf((obj) => obj.stock !== undefined)
    @Transform(({ value }) => Number(value))
    @IsNumber()
    @IsInt()
    @IsNotEmpty()
    @IsPositive()
    readonly stock?: number;
    
    readonly image?: string;

    @ValidateIf((obj) => obj.isActive !== undefined)
    @IsBoolean()
    @IsNotEmpty()
    readonly isActive: boolean;

    @UpdateDateColumn()
    readonly updatedAt?: Date;

    @ValidateIf((obj) => obj.categoryId !== undefined)
    @Transform(({ value }) => Number(value))
    @IsInt()
    @Min(1)
    readonly categoryId?: number;
}
