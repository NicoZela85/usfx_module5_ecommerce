import { Exclude } from "class-transformer";
import { Contains, IsInt, IsNotEmpty, IsNumber, IsPositive, IsString, Length, Min } from "class-validator";
import { Transform } from "class-transformer";

export class CreateProductDto {
    @IsString()
    @Length(4, 150)
    name: string;

    @Exclude()
    slug: string;

    @IsString()
    @IsNotEmpty()
    description: string;

    @IsNumber()
    @IsNotEmpty()
    @Transform(({ value }) => Number(value))
    @IsPositive()
    price: number;

    @IsNumber()
    @IsInt()
    @IsNotEmpty()
    @Transform(({ value }) => Number(value))
    @IsPositive()
    stock: number;
    
    image: string

    @IsInt()
    @Transform(({ value }) => Number(value))
    @Min(1)
    categoryId: number;
}
