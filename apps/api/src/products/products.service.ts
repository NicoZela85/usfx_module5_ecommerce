import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './product.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoriesService } from 'src/categories/categories.service';
import * as fs from 'fs';


@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productRepository: Repository<Product>,
    private categoriesService: CategoriesService
  ) {}

  async getProducts(page: number, limit: number, search: string, categoryId: number) {
    let queryBuilder = this.productRepository.createQueryBuilder('product')
                      .leftJoinAndSelect('product.category', 'category')
                      .skip((page - 1) * limit)
                      .take(limit);
                      
    if (search) {
      queryBuilder = queryBuilder
                      .where('(LOWER(product.name) LIKE LOWER(:search) OR LOWER(product.description) LIKE LOWER(:search))', { search: `%${search}%` });
    }

    if (categoryId) {
      queryBuilder = queryBuilder
                      .andWhere('category.id = :categoryId', { categoryId });
    }

    const [products, totalItems] = await queryBuilder.getManyAndCount();

    const totalPages = Math.ceil(totalItems / limit);
    return {
      data: products,
      meta: {
        totalItems,
        itemsPerPage: limit,
        currentPage: page,
        totalPages,
      },
    };
  }

  getProductById(id: number) {
    return this.productRepository.findOne({
      where: { id }, relations: { category: true },
    });
  }
 
  getProductBySlug(slug: string) {
    return this.productRepository.findOne({
      where: { slug }, relations: { category: true },
    });
  }

  async createProduct(product: CreateProductDto) {
    const { categoryId, image, ...productData } = product;
    const category = await this.categoriesService.getCategory(categoryId);
  
    const newProduct = this.productRepository.create({
      ...productData,
      image: image,
      category: category,
    });
  
    return await this.productRepository.save(newProduct);
  }
  

  async updateProduct(id: number, product: UpdateProductDto, image?: Express.Multer.File) {
    const { categoryId, ...productData } = product;
    const category = await this.categoriesService.getCategory(categoryId);
    const productUpdate =  await this.productRepository.findOne({ where: {id}});

    if (image) {
      if (productUpdate.image && fs.existsSync(productUpdate.image)) {
        fs.unlinkSync(productUpdate.image);
      }
      productUpdate.image = image.path;
    }
    productUpdate.updatedAt = new Date();
    productUpdate.category = category;
    Object.assign(productUpdate, productData);
  
    return this.productRepository.save(productUpdate); 
  }
  

  async deleteProduct(id: number) {
    const product = await this.productRepository.findOne({where: {id}})

    if (product.image && fs.existsSync(product.image)){
      fs.unlinkSync(product.image);
    }
    const deleteProduct = await this.productRepository.delete(id);
    return deleteProduct ? true : false;
  }

  async existsProduct(slug: string): Promise<boolean> {
    const existsProduct = await this.productRepository.findOne({ where: { slug } });
    return existsProduct ? true : false;
  }
}
