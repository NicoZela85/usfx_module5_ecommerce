import { Category } from "src/categories/category.entity";
import { BaseEntity } from "src/common/base.entity";
import { Column, Entity, ManyToOne } from "typeorm";

@Entity({ name: 'products'})
export class Product extends BaseEntity{
    @Column({ length: 150 })
    name: string;

    @Column({ length: 200, unique: true })
    slug: string;
    
    @Column("text")
    description: string;
    
    @Column({ type: 'decimal', precision: 10, scale: 2 })
    price: number;
    
    @Column("int")
    stock: number;
    
    @Column({ length: 250 })
    image: string;

    @ManyToOne(() => Category, category => category.products)
    category: Category;
}
