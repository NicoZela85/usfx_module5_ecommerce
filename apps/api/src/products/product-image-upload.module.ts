import { MulterModule } from '@nestjs/platform-express';
import { Module } from '@nestjs/common';
import { diskStorage } from 'multer';
import { extname, resolve } from 'path';

@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
      limits: { fileSize: 7 * 1024 * 1024 },
      fileFilter: (req, file, cb) => {
        if (file.mimetype.match(/\/(jpg|jpeg|png)$/)) {
          cb(null, true);
        } else {
          req.fileValidationError = 'Invalid file type';
          cb(null, false);
        }
      },
    }),
  ],
  exports: [MulterModule],
})
export class UploadModule {}
