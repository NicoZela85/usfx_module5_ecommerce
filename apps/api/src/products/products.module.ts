import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './product.entity';
import { CategoriesModule } from 'src/categories/categories.module';
import { UploadModule } from './product-image-upload.module';
import { FileUploadErrorMiddleware } from 'src/common/FileMiddleware';

@Module({
  imports: [TypeOrmModule.forFeature([Product]), CategoriesModule, UploadModule],
  controllers: [ProductsController],
  providers: [ProductsService],
})
export class ProductsModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
    .apply(FileUploadErrorMiddleware)
    .forRoutes(ProductsController)
  }
}
