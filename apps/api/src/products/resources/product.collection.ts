import { Product } from "../product.entity";

export class ProductCollection {
    constructor(private products: Product[], private meta: Object) {}
  
    format()  {
        return {
            type: 'products',
            meta: this.meta,
            data: this.products.map(product => ({
                id: product.id,
                name: product.name,
                slug: product.slug,
                description: product.description,
                price: product.price,
                stock: product.stock,
                image: product.image,
                IsActive: product.isActive,
                createdAt: product.createdAt,
                updatedAt: product.updatedAt,
                category: product.category.name,
                categoryId: product.category.id,
            })),
        };
    }
}
  