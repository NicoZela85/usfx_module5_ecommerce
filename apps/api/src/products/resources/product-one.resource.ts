import { Product } from "../product.entity";

export class ProductOneResource {
    constructor(private product: Product) {}
  
    format()  {
        return {
            data: {
                type: 'products',
                id: this.product.id,
                attributes: {
                    name: this.product.name,
                    slug: this.product.slug,
                    description: this.product.description,
                    price: this.product.price,
                    stock: this.product.stock,
                    image: this.product.image,
                    IsActive: this.product.isActive,
                    createdAt: this.product.createdAt,
                    updatedAt: this.product.updatedAt,
                    category: this.product.category.name,
                },
                relationships: {
                    "categoryId": this.product.category.id,
                }
            }
        };
    }
}
  