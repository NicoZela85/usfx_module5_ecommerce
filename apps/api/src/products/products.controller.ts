import { Controller, Get, Post, Body, Param, Delete, Query, ParseIntPipe, Patch, UseInterceptors, UploadedFile, ValidationPipe, Req } from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Roles } from 'nest-keycloak-connect';
import { stripHtmlTagsAndExtraSpaces } from 'src/utils/inputUtils';
import { toSlug } from 'src/utils/stringUtils';
import { ProductOneResource } from './resources/product-one.resource';
import { CategoriesService } from 'src/categories/categories.service';
import { handleDuplicateProductError, handleNonExistingProducError, handleNonExistingProducSlugError, handleSuccessfulDeletionResponse, handleInvalidFileTypeError, handleFileTooLargeError} from 'src/common/http-handling-product';
import { handleNonExistingCategoryError } from 'src/common/http-handling-category';
import { ProductCollection } from './resources/product.collection';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('/api/v1/products')
export class ProductsController {
  constructor(
    private readonly productsService: ProductsService,
    private readonly categoriesService: CategoriesService
  ) {}

  @Roles({ roles: ['admin', 'customer']})
  @Get()
  async getAll(
    @Query('page') page: string = '1', 
    @Query('limit') limit: string = '10',
    @Query('search') search: string = '',
    @Query('categoryId') categoryId: number = 0
  ) {
    const pageNumber = parseInt(page);
    const limitNumber = parseInt(limit);
    const searchProduct = stripHtmlTagsAndExtraSpaces(search);

    const products = await this.productsService.getProducts(pageNumber, limitNumber, searchProduct, categoryId);
    return new ProductCollection(products.data, products.meta).format();
  }

  @Roles({ roles: ['admin', 'customer']})
  @Get('/:id')
  async getOneById(@Param('id', ParseIntPipe) id: number) {
    const product = await this.productsService.getProductById(id);
    if (!product) {
      return handleNonExistingProducError();
    }

    return new ProductOneResource(product).format();
  }

  @Roles({ roles: ['admin', 'customer']})
  @Get('/name/:slug')
  async getOneBySlug(@Param('slug') slug: string) {
    const product = await this.productsService.existsProduct(slug);
    if (!product) {
      return handleNonExistingProducSlugError();
    }

    const response = await this.productsService.getProductBySlug(slug);
    return new ProductOneResource(response).format();
  }
  
  @Roles({ roles: ['admin'] })
  @Post()
  @UseInterceptors(FileInterceptor('image'))
  async create(@UploadedFile() file, @Body(new ValidationPipe({ transform: true })) newProduct: CreateProductDto, @Req() req) {
    if(req.fileValidationError){
      if(req.fileValidationError === 'Invalid file type'){
        return handleInvalidFileTypeError();
      }
      if (req.fileValidationError === 'File too large'){
        return handleFileTooLargeError();
      }
    }
    const name = stripHtmlTagsAndExtraSpaces(newProduct.name);
    const slug = toSlug(name);
  
    if (await this.productsService.existsProduct(slug)) {
      return handleDuplicateProductError();
    }
  
    if (!await this.categoriesService.getCategory(newProduct.categoryId)) {
      return handleNonExistingCategoryError();
    }
  
    const productToCreate = { ...newProduct, name, slug, image: file.path };
    const product = await this.productsService.createProduct(productToCreate);
  
    return new ProductOneResource(product).format();
  }

  @Roles({roles:['admin']})
  @Patch('/:id')
  @UseInterceptors(FileInterceptor('image'))
  async update(@Param('id', ParseIntPipe) id: number, @UploadedFile() file, @Body(new ValidationPipe ({transform: true})) product: UpdateProductDto, @Req() req) {
    const existingProduct = await this.productsService.getProductById(id);
    const productData = { ...product };

    if(req.fileValidationError){
      if(req.fileValidationError === 'Invalid file type'){
        return handleInvalidFileTypeError();
      }
      if (req.fileValidationError === 'File too large'){
        return handleFileTooLargeError();
      }
    }
  
    if (!existingProduct) {
      return handleNonExistingProducError();
    }
  
    if (productData.categoryId !== undefined) {
      if (!await this.categoriesService.getCategory(productData.categoryId)) {
        return handleNonExistingCategoryError();
      }
    }
  
    if (productData.name !== undefined) {
      const name = stripHtmlTagsAndExtraSpaces(productData.name);
      const slug = toSlug(name);
  
      if (existingProduct.slug !== slug) {
        if (await this.productsService.existsProduct(slug)) {
          return handleDuplicateProductError();
        }
      }
      productData.name = name;
      productData.slug = slug;
    }
  
    const updatedProduct = await this.productsService.updateProduct(id, productData, file);
    return new ProductOneResource(updatedProduct).format();
  }
  

  @Roles({ roles: ['admin']})
  @Delete(':id')
  async remove(@Param('id', ParseIntPipe) id: number) {
    const existingProduct = await this.productsService.getProductById(id);

    if (!existingProduct) {
      return handleNonExistingProducError();
    }

    const result = await this.productsService.deleteProduct(id);
    if (!result) {
      return handleNonExistingProducError();
    }
    return handleSuccessfulDeletionResponse();
  }
}
