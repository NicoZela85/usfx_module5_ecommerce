import { Entity, PrimaryGeneratedColumn, OneToMany, Column } from "typeorm";
import { ShoppingCartItem } from "./shopping-cart-item.entity";

@Entity()
export class ShoppingCart {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToMany(() => ShoppingCartItem, item => item.cart, { 
    cascade: true })
  items: ShoppingCartItem[];

  @Column('decimal', { precision: 10, scale: 2, default: 0 })
  totalCartPrice: number;

  @Column('uuid') 
  userId: string;
}