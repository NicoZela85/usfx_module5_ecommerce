import { Transform } from "class-transformer";
import { IsInt, IsPositive } from "class-validator";

export class AddItemDto {
    @IsInt()
    @Transform(({ value }) => Number(value))
    @IsPositive()
    productId: number;

    @IsInt()
    @Transform(({ value }) => Number(value))
    @IsPositive()
    quantity: number;
}
