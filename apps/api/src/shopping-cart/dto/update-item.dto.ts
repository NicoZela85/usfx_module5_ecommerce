import { Transform } from 'class-transformer';
import { IsInt, Min } from 'class-validator';

export class UpdateItemDto{
    @IsInt()
    @Transform(({ value }) => Number(value))
    @Min(0)
    quantity: number;
}
