  import { Injectable, NotFoundException } from '@nestjs/common';
  import { InjectRepository } from '@nestjs/typeorm';
  import { Product } from 'src/products/product.entity';
  import { ShoppingCartItem } from './entities/shopping-cart-item.entity';
  import { ShoppingCart } from './entities/shopping-cart.entity';
  import { Repository } from 'typeorm';
  import { AddItemDto } from './dto/add-item.dto';
  import { UpdateItemDto } from './dto/update-item.dto';

  @Injectable()
  export class ShoppingCartService {
    constructor(
      @InjectRepository(ShoppingCartItem)
      private readonly cartItemRepository: Repository<ShoppingCartItem>,
      @InjectRepository(ShoppingCart)
      private readonly shoppingCartRepository: Repository<ShoppingCart>,
      @InjectRepository(Product)
      private readonly productRepository: Repository<Product>,
    ) {}

    async addItemToCart(userId: string, addItemDto: AddItemDto): Promise<ShoppingCartItem> {
      const { productId, quantity } = addItemDto;
    
      const product = await this.productRepository.findOne({ where: { id: productId } });
      if (!product) {
        throw new NotFoundException(`Product with ID "${productId}" not found`);
      }
      if (product.stock < quantity) {
        throw new NotFoundException(`Not enough stock for product ID "${productId}"`);
      }
    
      let shoppingCart = await this.shoppingCartRepository.findOne({ 
        where: { userId },
        relations: ['items', 'items.product']
      });
    
      if (!shoppingCart) {
        shoppingCart = this.shoppingCartRepository.create({ userId, totalCartPrice: 0 });
      }
    
      let cartItem = shoppingCart.items.find(item => item.product.id === productId);
    
      if (cartItem) {
        cartItem.quantity += quantity;
        cartItem.totalPrice = parseFloat((cartItem.product.price * cartItem.quantity).toFixed(2));
      } else {
        cartItem = this.cartItemRepository.create({
          product,
          quantity,
          totalPrice: parseFloat((product.price * quantity).toFixed(2)),
          cart: shoppingCart,
        });
        shoppingCart.items.push(cartItem);
      }
    
      await this.cartItemRepository.save(cartItem);
      await this.recalculateAndSaveCart(shoppingCart);
    
      return cartItem;
    }
    
    async getCartItems(userId: string): Promise<ShoppingCartItem[]> {
      let shoppingCart = await this.shoppingCartRepository.findOne({ 
        where: { userId },
        relations: ['items', 'items.product']
      });

      if (!shoppingCart) {
        shoppingCart = this.shoppingCartRepository.create({ userId });
        await this.shoppingCartRepository.save(shoppingCart);
        return [];
        }

      return shoppingCart.items;
    }

    async removeItemFromCart(userId: string, itemId: number): Promise<void> {
      const item = await this.cartItemRepository.findOne({
        where: { id: itemId },
        relations: ['cart'],
      });

      if (!item) {
        throw new NotFoundException(`Item with ID "${itemId}" not found`);
      }

      if (item.cart.userId !== userId) {
        throw new Error('Unauthorized access to cart item');
      }

      await this.cartItemRepository.remove(item);
    }

    async updateItemQuantity(userId: string, itemId: number, updateDto: UpdateItemDto): Promise<ShoppingCartItem> {
      const item = await this.cartItemRepository.findOne({
        where: { id: itemId },
        relations: ['cart', 'product'],
      });

      if (!item) {
        throw new NotFoundException(`Item with ID "${itemId}" not found`);
      }

      if (item.cart.userId !== userId) {
        throw new Error('Unauthorized access to cart item');
      }

      if (updateDto.quantity === 0) {
        await this.cartItemRepository.remove(item);
        await this.recalculateAndSaveCart(item.cart);
        return null; 
      }

      if (item.product.stock < updateDto.quantity) {
        throw new NotFoundException(`Not enough stock for product ID "${item.product.id}"`);
      }

      item.quantity = updateDto.quantity;
      item.totalPrice = parseFloat((item.product.price * item.quantity).toFixed(2));

      await this.cartItemRepository.save(item);
      await this.recalculateAndSaveCart(item.cart);

      return item;
    }

    private async recalculateAndSaveCart(cart: ShoppingCart): Promise<void> {
      const items = await this.cartItemRepository.find({ where: { cart: { id: cart.id } } });

      const newTotal = items.reduce((sum, item) => {
          const itemTotal = parseFloat(item.totalPrice.toString()); 
          return sum + itemTotal; 
      }, 0.00); 
      
      cart.totalCartPrice = parseFloat(newTotal.toFixed(2));
      await this.shoppingCartRepository.save(cart);
  }

    async clearCart(userId: string) {
      let shoppingCart = await this.findUserShoppingCart(userId);
      if (!shoppingCart) {
        return;
      }
      shoppingCart.items = [];
      shoppingCart.totalCartPrice = 0;
      await this.shoppingCartRepository.save(shoppingCart);
    }

    private async findUserShoppingCart(userId: string) {
      return await this.shoppingCartRepository.findOne({
        where: { userId },
        relations: ['items', 'items.product']
      });
    }

    async totalCartPrice(userId: string): Promise<number> {
      let shoppingCart = await this.shoppingCartRepository.findOne({ where: { userId } });
      if (!shoppingCart) {
        return 0;
      }
      return shoppingCart.totalCartPrice;
    }
  }