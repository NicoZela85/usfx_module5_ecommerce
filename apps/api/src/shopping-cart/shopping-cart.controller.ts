import { Controller, Get, Post, Body, Patch, Param, Delete, Req, ValidationPipe } from '@nestjs/common';
import { ShoppingCartService } from './shopping-cart.service';
import { AddItemDto } from './dto/add-item.dto';
import { Roles } from 'nest-keycloak-connect';
import { ShoppingCartItem } from './entities/shopping-cart-item.entity';
import { UpdateItemDto } from './dto/update-item.dto';

@Controller('/api/v1/shopping-cart')
export class ShoppingCartController {
  constructor(private readonly shoppingCartService: ShoppingCartService) { }

  @Roles({ roles: ['customer'] })
  @Post('item')
  async addItemToCart(@Body(new ValidationPipe({ transform: true })) addItemDto: AddItemDto, @Req() req) {
    const userId = req.userId; 
    if (!userId) {
      throw new Error('User not authenticated');
    }
    return await this.shoppingCartService.addItemToCart(userId, addItemDto);
  }

  @Roles({ roles: ['customer']})
  @Get('items')
  async getCartItems(@Req() req): Promise<ShoppingCartItem[]> {
    const userId = req.userId;
    if (!userId) {
      throw new Error('User not authenticated');
    }
    return await this.shoppingCartService.getCartItems(userId);
  }

  @Roles({ roles: ['customer'] })
  @Delete('item/:itemId')
  async removeItemFromCart(@Param('itemId') itemId: number, @Req() req) {
    const userId = req.userId;
    if (!userId) {
      throw new Error('User not authenticated');
    }
    return await this.shoppingCartService.removeItemFromCart(userId, itemId);
  }

  @Roles({ roles: ['customer'] })
  @Patch('item/:itemId')
  async updateItemQuantity(@Param('itemId') itemId: number, @Body() updateDto: UpdateItemDto, @Req() req) {
    const userId = req.userId;
    if (!userId) {
      throw new Error('User not authenticated');
    }
    return await this.shoppingCartService.updateItemQuantity(userId, itemId, updateDto);
  }
}
