import React, { useState } from 'react';
import { CreateProductDto } from '../../../../api/src/products/dto/create-product.dto';
import { useSession } from 'next-auth/react';

const ProductCreateForm = () => {
  const [formData, setFormData] = useState<Partial<CreateProductDto>>({
    name: '',
    description: '',
    price: 0,
    stock: 0,
    categoryId: 0,
  });
  const [imageFile, setImageFile] = useState<File | null>(null);
  const { data: session } = useSession();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      setImageFile(file);
    }
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log(formData);

    if (!session?.accessToken) {
      console.error('No hay token de acceso disponible');
      return;
    }

    try {
      const formDataWithImage = new FormData();
      formDataWithImage.append('name', formData.name || '');
      formDataWithImage.append('description', formData.description || '');
      formDataWithImage.append('price', String(formData.price || 0));
      formDataWithImage.append('stock', String(formData.stock || 0));
      formDataWithImage.append('categoryId', String(formData.categoryId || 0));
      if (imageFile) {
        formDataWithImage.append('image', imageFile);
      }

      const response = await fetch('http://localhost:4000/api/v1/products', {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${session.accessToken}`
        },
        body: formDataWithImage,
      });

      if (response.ok) {
        console.log('Producto creado exitosamente');
        setFormData({
          name: '',
          description: '',
          price: 0,
          stock: 0,
          categoryId: 0,
        });
        setImageFile(null);
      } else {
        console.error('Error al crear el producto');
      }
    } catch (error) {
      console.error('Error en la solicitud:', error);
    }
  };

  return (
    <div className="flex flex-col items-center justify-center bg-white p-4 rounded-lg shadow-md">
      <h2 className="text-gray-800 text-2xl font-semibold mb-4">Create Product</h2>
      <form onSubmit={handleSubmit} className="grid grid-cols-2 gap-4 w-full max-w-lg">
        <div>
          <label htmlFor="name" className="text-gray-700">Name</label>
          <input
            type="text"
            name="name"
            placeholder="Enter product name"
            value={formData.name}
            onChange={handleChange}
            className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
            required
          />
        </div>
        <div>
          <label htmlFor="description" className="text-gray-700">Description</label>
          <input
            type="text"
            name="description"
            placeholder="Enter product description"
            value={formData.description}
            onChange={handleChange}
            className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
            required
          />
        </div>
        <div>
          <label htmlFor="price" className="text-gray-700">Price</label>
          <input
            type="number"
            name="price"
            placeholder="Enter product price"
            value={formData.price}
            onChange={handleChange}
            className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
            required
          />
        </div>
        <div>
          <label htmlFor="stock" className="text-gray-700">Stock</label>
          <input
            type="number"
            name="stock"
            placeholder="Enter product stock"
            value={formData.stock}
            onChange={handleChange}
            className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
            required
          />
        </div>
        <div>
          <label htmlFor="image" className="text-gray-700">Image</label>
          <input
            type="file"
            accept=".jpg,.jpeg,.png"
            name="image"
            onChange={handleImageChange}
            className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
            required
          />
        </div>
        <div>
          <label htmlFor="categoryId" className="text-gray-700">Category ID</label>
          <input
            type="number"
            name="categoryId"
            placeholder="Enter product category ID"
            value={formData.categoryId}
            onChange={handleChange}
            className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
            required
          />
        </div>
        <button type="submit" className="col-span-2 bg-blue-700 text-white py-2 px-4 rounded-md hover:bg-blue-600 transition-colors duration-300">Create Product</button>
      </form>
    </div>
  );
}

export default ProductCreateForm;
