import React from "react";
import SignInButton from "./SignInButton";

export default function ContentSignOut() {
    return (
        <div className="flex justify-center items-center h-screen">
            <div className="flex flex-col items-center justify-center h-screen">
                <h1 className="text-4xl font-bold mb-8 text-center">Welcome to SupaMark!</h1>
                <span className="text-lg mb-8">Explore products and find great deals.</span>
                <div className="text-center"> { }
                    <div className="my-8"> { }
                        <SignInButton />
                        <span className="text-lg mb-8 font-bold">Log in now!</span>
                    </div>
                </div>
            </div>
        </div>
    );
}