"use client"
import Image from 'next/image';
import Logout from '../images/logout.png';
import federatedLogout from "../utils/federatedLogout";

export default function Login(): JSX.Element {
  return <button className="flex items-center justify-center w-full py-2 px-4 bg-gray-200 text-gray-800 rounded-md hover:bg-gray-300 hover:text-black focus:outline-none"
   onClick={() => federatedLogout()}>
    <Image src={Logout} alt="Button Image" width={32} height={32} className="mr-3" />
      <span className="text-lg font-bold">Sign out</span>
  </button>
}