"use client";
import React from 'react'
import Image from 'next/image';
import Image1 from '../images/image.png';
import { TypeAnimation } from 'react-type-animation';

const Welcome= ()=> {
  return (<section>
    <div className=" elative pt-2 lg:pt-2 min-h-screen grid grid-cols-1 sm:grid-cols-12">
      <div className='col-span-7 place-self-center text-center sm:text-left'> 
        <h1 className="text-black mb-4 text-4xl sm:text-6xl lg:text-6xl front-extrabold">
            <span className="text-transparent bg-clip-text bg-gradient-to-r from-black to-[#949494] ">{" "}
                FIND CLOTHES THAT MATCHES YOUR STYLE
            </span>
              <br></br>
            <TypeAnimation 
              sequence={[
                "Jeans..",
                1000,
                "T-shirts..",
                1000,
                "Shoes..",
                1000,
              ]}
              wrapper='span'
              speed={50}
                repeat={Infinity}
            />
            
        </h1>
        <p className="text-[#808080] text-base sm:text-lg mb-6 lg:text-xl">
          Immerse yourself in a world of trends, quality and comfort as you explore our carefully curated collection of clothing.
          From elegant outfits to casual pieces and fashion accessories, we have everything you need to express your style for every occasion.
        </p>
      </div>
      
      <div className="col-span-5 place-self-center mt-4 lg:mt-0">
         <div className="rounded-full bg-black w-[250px] h-[250px] lg:w-[415px] lg:h-[415px] relative">
            <Image
              src={Image1.src}
              alt="image 1"
              className="absolute transform -translate-x-1/2 -translate-y-1/2  top-1/2 left-1/2"
              height={300}
              width={300}
            />
         </div>
         
      </div>
    </div>
   
  </section> 
  );
};
export default Welcome

