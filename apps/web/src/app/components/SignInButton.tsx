"use client"
import Image from 'next/image';
import market from '../images/market.png';
import { signIn } from "next-auth/react";

export default function Login(): JSX.Element{
  return <button className="w-full py-2 px-4 bg-gray-200 text-gray-800 rounded-md hover:bg-gray-300 hover:text-black focus:outline-none"
   onClick={() => signIn("keycloak")}>
    {/* <Image src={market} alt="Button Image" width={32} height={32} className="mr-3" /> */}
      <span className="text-lg font-bold">Sign Up / Log In</span>
  </button>
}