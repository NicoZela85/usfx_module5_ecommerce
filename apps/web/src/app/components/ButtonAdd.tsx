import Link from "next/link";

const ButtonAdd = () => {
  return (
    <Link href="/pages/admin/createFormCat">
      <button className="px-5 py-3 text-white duration-150 bg-sky-500 rounded-full hover:bg-sky-500 active:bg-sky-700">
        + Add Category
      </button>
    </Link>

  );
}
export default ButtonAdd;