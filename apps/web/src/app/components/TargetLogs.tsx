import { signIn, signOut } from "next-auth/react";

import logo from "../app/assets/logo.png";

interface TargetLogsProps {
  buttonText: string;
  authAction: "signIn" | "signOut";
}

export function TargetLogs({ buttonText, authAction }: TargetLogsProps) {
  const handleAuthAction = () => {
    if (authAction === "signIn") {
      signIn("keycloak");
    } else if (authAction === "signOut") {
      signOut({ callbackUrl: "/login" });
    }
  };

  return (
    <div className="target-logs-container">
      <div className="border-2 border-solid border-[#ccc] rounded-lg p-12 text-center bg-white shadow-[0_4px_6px_rgba(0,0,0,0.1)]">
        <img src={logo.src} alt="Logo" className="image" />
        <h1 className="text-2xl mb-4 text-center">
          {authAction === "signIn" ? "Welcome!" : "Goodbye!"}
        </h1>
        <button
          className={authAction === "signIn" ? "bg-[#ED7A56] text-white border-none rounded px-2.5 py-5 text-base cursor-pointer" : "logout-button"}
          onClick={handleAuthAction}
        >
          {buttonText}
        </button>
      </div>
    </div>
  );
}
