"use client";
import React from 'react'


const Products = () => {
    return (
        <section>
            <div className="relative pt-2 lg:pt-2 min-h-screen">
                <div className="bg-cover w-full flex justify-center items-center"
                    style={{ backgroundImage: "url('/images/mybackground.jpeg')" }}>
                    <div className="w-full bg-white p-5  bg-opacity-40 backdrop-filter backdrop-blur-lg">
                        <div className="w-12/12 mx-auto rounded-2xl bg-white p-5 bg-opacity-40 backdrop-filter backdrop-blur-lg">
                            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-3 text-center px-2 mx-auto">
                                <article
                                    className="bg-white  p-6 mb-6 shadow transition duration-300 group transform hover:-translate-y-2 hover:shadow-2xl rounded-2xl cursor-pointer border">
                                    <a target="_self" href="/pages"
                                        className="absolute opacity-0 top-0 right-0 left-0 bottom-0"></a>
                                    <div className="relative mb-4 rounded-2xl">
                                        <img className="max-h-80 rounded-2xl w-full object-cover transition-transform duration-300 transform group-hover:scale-105"
                                            src="https://sucre.solutekla.com/photo/1/samsung/celulares/celular_galaxy_a20s_32gb_negro/celular_galaxy_a20s_32gb_negro_0001" alt="" />
                                        <a className="flex justify-center items-center bg-black bg-opacity-80 z-10 absolute top-0 left-0 w-full h-full text-white rounded-2xl opacity-0 transition-all duration-300 transform group-hover:scale-105 text-xl group-hover:opacity-100"
                                            href="/pages" target="_self" rel="noopener noreferrer">
                                            Read description
                                            <svg className="ml-2 w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                    d="M13 5l7 7-7 7M5 5l7 7-7 7"></path>
                                            </svg>
                                        </a>
                                    </div>
                                    <div className="flex justify-between items-center w-full pb-4 mb-auto">
                                        <div className="flex items-center">
                                            <div className="flex flex-1">
                                                <div className="">
                                                    <p className="text-sm font-semibold ">clothes....</p>
                                                </div>
                                            </div>
                                        </div>
<<<<<<< HEAD
                                        <div className="flex justify-end">
                                            <div className="text-sm flex items-center text-[#5f5f5f] ">
                                                Price: 200
                                            </div>
=======
                                    <div className="flex justify-end">
                                        <div className="text-sm flex items-center text-[#060606] ">
                                            Price: 200
>>>>>>> 3ebd6cb0ea219ec8a107ab016f1f40615bb6db16
                                        </div>
                                    </div>
                                    <h3 className="font-medium text-xl leading-8">
                                        <a href="/blog/slug"
                                            className="block relative group-hover:text-[#838383] transition-colors duration-200 ">
                                            add to shopping card
                                        </a>
                                    </h3>
                                    <div>
                                    </div>
                                </article>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Products

