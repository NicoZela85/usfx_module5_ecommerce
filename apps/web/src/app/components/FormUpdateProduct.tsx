
"use client"
import React, { useState, useEffect } from 'react';
import { UpdateProductDto } from '../../../../api/src/products/dto/update-product.dto';
import { useSession } from 'next-auth/react';


const ProductUpdateForm = () => {
    const [formData, setFormData] = useState<Partial<UpdateProductDto>>({
        name: '',
        description: '',
        price: 0,
        stock: 0,
        image: '',
        categoryId: 0,
    });

    const [productId, setProductId] = useState('');

    const { data: session } = useSession(); // Obtiene la sesión del usuario

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        console.log(formData);

        if (!session?.accessToken) {
            // Manejar caso donde no hay token de acceso
            console.error('No hay token de acceso disponible');
            return;
        }

        try {
            const response = await fetch(`http://localhost:4000/api/v1/products/${productId}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${session.accessToken}`
                },
                body: JSON.stringify(formData),
            });

            if (response.ok) {
                console.log('Producto actualizado exitosamente');
                setFormData({
                    name: '',
                    description: '',
                    price: 0,
                    stock: 0,
                    image: '',
                    categoryId: 0,
                });
                setProductId('');
            } else {
                console.error('Error al actualizar el producto');
            }
        } catch (error) {
            console.error('Error en la solicitud:', error);
        }
    };

    useEffect(() => {
        // Aquí puedes realizar una llamada al servidor para obtener los datos del producto
        // que deseas actualizar y establecer el estado inicial de formData con esos datos
        const fetchProductData = async () => {
            if (!productId) return;

            try {
                const response = await fetch(`http://localhost:4000/api/v1/products/${productId}`, {
                    headers: {
                        'Authorization': `Bearer ${session?.accessToken}`
                    }
                });

                if (response.ok) {
                    const product = await response.json();
                    setFormData(product);
                } else {
                    console.error('Error al obtener los datos del producto');
                }
            } catch (error) {
                console.error('Error en la solicitud:', error);
            }
        };

        fetchProductData();
    }, [productId, session?.accessToken]);

    return (
        <div className="flex flex-col items-center justify-center bg-white p-4 rounded-lg shadow-md">
            <h2 className="text-gray-800 text-2xl font-semibold mb-4">Update Product</h2>
            <form onSubmit={handleSubmit} className="grid grid-cols-2 gap-4 w-full max-w-lg">
                <div>
                    <label htmlFor="productId" className="text-gray-700">Product ID</label>
                    <input
                        type="text"
                        name="productId"
                        placeholder="Enter product ID"
                        value={productId}
                        onChange={(e) => setProductId(e.target.value)}
                        className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
                        required
                    />
                </div>
                <div>
                    <label htmlFor="name" className="text-gray-700">Name</label>
                    <input
                        type="text"
                        name="name"
                        placeholder="Enter product name"
                        value={formData.name}
                        onChange={handleChange}
                        className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
                        required
                    />
                </div>
                <div>
                    <label htmlFor="description" className="text-gray-700">Description</label>
                    <input
                        type="text"
                        name="description"
                        placeholder="Enter product description"
                        value={formData.description}
                        onChange={handleChange}
                        className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
                        required
                    />
                </div>
                <div>
                    <label htmlFor="price" className="text-gray-700">Price</label>
                    <input
                        type="number"
                        name="price"
                        placeholder="Enter product price"
                        value={formData.price}
                        onChange={handleChange}
                        className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
                        required
                    />
                </div>
                <div>
                    <label htmlFor="stock" className="text-gray-700">Stock</label>
                    <input
                        type="number"
                        name="stock"
                        placeholder="Enter product stock"
                        value={formData.stock}
                        onChange={handleChange}
                        className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
                        required
                    />
                </div>
                <div>
                    <label htmlFor="image" className="text-gray-700">Image URL</label>
                    <input
                        type="text"
                        name="image"
                        placeholder="Enter product image URL"
                        value={formData.image}
                        onChange={handleChange}
                        className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
                        required
                    />
                </div>
                <div>
                    <label htmlFor="categoryId" className="text-gray-700">Category ID</label>
                    <input
                        type="number"
                        name="categoryId"
                        placeholder="Enter product category ID"
                        value={formData.categoryId}
                        onChange={handleChange}
                        className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
                        required
                    />
                </div>
                <button type="submit" className="col-span-2 bg-blue-700 text-white py-2 px-4 rounded-md hover:bg-blue-600 transition-colors duration-300">Update Product</button>
            </form>
        </div>
    );
};

export default ProductUpdateForm;