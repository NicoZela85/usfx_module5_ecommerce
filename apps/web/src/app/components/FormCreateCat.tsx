"use client"
import React, { useState } from 'react';
import { CreateProductDto } from '../../../../api/src/products/dto/create-product.dto';
import { CreateCategoryDto } from '../../../../api/src/categories/dto/create-category.dto';
import { useSession } from 'next-auth/react';

const CategoryCreateForm = () => {
  const [formData, setFormData] = useState("");

  const { data: session } = useSession(); // Obtiene la sesión del usuario

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log(formData);

    if (!session?.accessToken) {
      // Manejar caso donde no hay token de acceso
      console.error('No hay token de acceso disponible');
      return;
    }

    try {
      const response = await fetch('http://localhost:4000/api/v1/products', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${session.accessToken}`
        },
        body: JSON.stringify({
          name: formData,
        })
      });

      if (response.ok) {
        console.log('Producto creado exitosamente');
      } else {
        console.error('error to create the category');
      }
    } catch (error) {
      console.error('Error en la solicitud:', error);
    }
  };

  return (
    <div className="flex flex-col items-center justify-center bg-white p-4 rounded-lg shadow-md">
      <h2 className="text-gray-800 text-2xl font-semibold mb-4">Create category</h2>
      <form onSubmit={handleSubmit} className="grid grid-cols-2 gap-4 w-full max-w-lg">
        <div>
          <label htmlFor="name" className="text-gray-700">Name</label>
          <input
            type="text"
            name="name"
            placeholder="Enter category name"
            onChange={(e) => setFormData(e.target.value)}
            value={formData}
            className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
            required
          />
        </div>
        <button type="submit" className="col-span-2 bg-blue-700 text-white py-2 px-4 rounded-md hover:bg-blue-600 transition-colors duration-300">Create category</button>
      </form>
    </div>
  );
};

export default CategoryCreateForm;
