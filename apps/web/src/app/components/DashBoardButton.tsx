"use client"
import Image from 'next/image';
import DashBoardLogo from "../images/DashBoard Logo.png";
import Link from "next/link";
import { useSession } from 'next-auth/react';

const goToDashBoard = () => {
    console.log("Clicked on DashBoard button");
    window.location.href = '/pages/admin';
};
export default function DashBoardButton() {
    return (
        <button className="flex items-center justify-center w-full py-2 px-4 bg-gray-200 text-gray-800 rounded-md hover:bg-gray-300 hover:text-black focus:outline-none"
            onClick={() => goToDashBoard()}>
            <Image src={DashBoardLogo} alt="Button Image" width={32} height={32} className="mr-3" />
            <span className="text-lg font-bold">Go DashBoard</span>
        </button>
    );
}