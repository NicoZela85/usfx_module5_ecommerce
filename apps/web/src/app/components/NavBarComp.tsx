"use client";
import React, { useState, useEffect } from 'react';
import profileImage from '../images/profile.jpg';
import SearchBar from '../components/SearchComp';
import marketImage from '../images/market.png';
import { useSession } from 'next-auth/react';
import SignOutButton from "../components/SignOutButton"
import DashBoardButton from "../components/DashBoardButton"
import jwt from 'jsonwebtoken';

const goToCartPage = () => {
  console.log("Clicked on Shopping Cart button");
  window.location.href = 'CartPage.tsx';
};
const goToHome = () => {
  console.log("Clicked on Shopping Cart button");
  window.location.href = '/';
};

const NavBarComp = () => {
  const [isNavbarOpen, setIsNavbarOpen] = useState(false);
  const { data: session } = useSession();
  const [userRole, setUserRole] = useState<string | undefined>(undefined);
  const [isAdmin, setIsAdmin] = useState<boolean | undefined>(undefined);

  // get the role
  useEffect(() => {
    if (session && session.accessToken) {
      const decodedToken = jwt.decode(session.accessToken);
      if (decodedToken && typeof decodedToken !== 'string') {
        const roles = decodedToken?.resource_access;
        console.log(roles);
        const firstRole = roles ? Object.keys(roles)[0] : undefined;
        const role = firstRole ? roles[firstRole].roles[0] : undefined;
        const isAdmin = role === 'admin';
        setIsAdmin(isAdmin);
        setUserRole(role);
      }
    }
  }, [session]);

  const toggleNavbar = () => {
    setIsNavbarOpen(!isNavbarOpen);
  };

  return (
    <div className="relative bg-[#FFFF] text-black z-10">
      <div className="flex justify-between items-center p-5">
        <div className="flex items-center space-x-6">
          <img
            src={marketImage.src}
            alt="Market"
            className="w-12 h-12 object-cover"
            onClick={goToHome}
          />
          <a href="/pages/Shop" className="uppercase">Shop</a>
          <button onClick={goToCartPage} className="uppercase">Shopping Cart</button>
          <button className="uppercase">New Arrivals</button>
          <a href="/pages/Categories" className="uppercase">Categories</a>
        </div>
        <SearchBar />
        <div>
          <img
            src={profileImage.src}
            alt="Profile"
            className="w-12 h-12 rounded-full object-cover cursor-pointer"
            onClick={toggleNavbar}
          />
        </div>
      </div>
      {/* Navbar desplegable */}
      {isNavbarOpen && (
        <div className="absolute top-0 right-0 h-auto w-80 bg-white shadow-lg overflow-hidden transition-transform transform translate-x-0">
          <div className="flex flex-col h-full">
            <div className="flex justify-between items-center p-4 border-b border-gray-300">
              <h3 className="text-lg font-semibold text-gray-800">User information</h3>
              <button onClick={toggleNavbar} className="text-gray-600 hover:text-black focus:outline-none">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                </svg>
              </button>
            </div>
            <div className="flex flex-col p-4 space-y-2">
              <div className="flex items-center space-x-2">
                <span className="text-gray-600">Name:</span>
                <span className="text-gray-800">{session?.user?.name || 'Usuario no identificado'}</span>
              </div>
              <div className="flex items-center space-x-2">
                <span className="text-gray-600">Role:</span>
                <span className="text-gray-800">{userRole || 'Rol no disponible'}</span>
              </div>
            </div>
            { isAdmin && (
              <DashBoardButton />
            )}
            <div className="p-4">
            <SignOutButton />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default NavBarComp;

