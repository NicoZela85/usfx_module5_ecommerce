"use client";
import React from 'react';
import { useSearchParams } from 'next/navigation';

interface PaginationProps {
    totalPages: number;
    currentPage: number;
    currentSearch: string;
    categoryId: number;
}

const Pagination: React.FC<PaginationProps> = ({ totalPages, currentPage, currentSearch, categoryId }) => {
    const searchParams = useSearchParams();
    currentSearch = searchParams.get('search') ?? '';
    categoryId = searchParams.get('categoryId') ? parseInt(searchParams.get('categoryId') as string) : 0;

    const getPageButtons = () => {
        const buttons = [];
        const numAdjacentButtons = 2; // Number of adjacent buttons to show on each side of the current page
        let startPage = Math.max(1, currentPage - numAdjacentButtons);
        let endPage = Math.min(totalPages, currentPage + numAdjacentButtons);

        if (currentPage <= numAdjacentButtons + 1) {
            endPage = Math.min(totalPages, numAdjacentButtons * 2 + 1);
        } else if (currentPage >= totalPages - numAdjacentButtons) {
            startPage = Math.max(1, totalPages - numAdjacentButtons * 2);
        }

        if (startPage > 1) {
            buttons.push(
                <button
                    key={1}
                    onClick={() => goToPage(1, currentSearch, categoryId)}
                    className="bg-transparent hover:bg-gray-500 text-white font-semibold py-2 px-4 border border-white hover:border-transparent rounded"
                >
                    1
                </button>
            );
            buttons.push(<span key="ellipsis-start">...</span>);
        }

        for (let i = startPage; i <= endPage; i++) {
            buttons.push(
                <button
                    key={i}
                    onClick={() => goToPage(i, currentSearch, categoryId)}
                    className={`bg-transparent hover:bg-gray-500 text-white font-semibold py-2 px-4 border border-white hover:border-transparent rounded ${i === currentPage ? 'bg-gray-500' : ''}`}
                >
                    {i}
                </button>
            );
        }

        if (endPage < totalPages) {
            buttons.push(<span key="ellipsis-end">...</span>);
            buttons.push(
                <button
                    key={totalPages}
                    onClick={() => goToPage(totalPages, currentSearch, categoryId)}
                    className="bg-transparent hover:bg-gray-500 text-white font-semibold py-2 px-4 border border-white hover:border-transparent rounded"
                >
                    {totalPages}
                </button>
            );
        }

        return buttons;
    };

    const goToPage = (page: number, search: string, categoryId: number) => {
        console.log("Clicked on Shopping Cart button");
        window.location.href = `/pages/Shop?page=${page}&search=${search}&categoryId=${categoryId}`;
    };

    return (
        <section>
            <footer className="footer footer-center p-1 bg-slate-500 text-white justify-center w-full">
                <nav>
                    <div className="grid grid-flow-col gap-4 justify-center">
                        <button
                            onClick={() => goToPage(currentPage - 1 > 0 ? currentPage - 1 : totalPages, currentSearch, categoryId)}
                            className="bg-transparent hover:bg-gray-500 text-white font-semibold py-2 px-4 border border-white hover:border-transparent rounded"
                        >
                            <span>&lt; Previous</span>
                        </button>
                        {getPageButtons()}
                        <button
                            onClick={() => goToPage(currentPage + 1 <= totalPages ? currentPage + 1 : 1, currentSearch, categoryId)}
                            className="bg-transparent hover:bg-gray-500 text-white font-semibold py-2 px-4 border border-white hover:border-transparent rounded"
                        >
                            <span>Next &gt;</span>
                        </button>
                    </div>
                </nav>
            </footer>
        </section>
    );
};

export default Pagination;
