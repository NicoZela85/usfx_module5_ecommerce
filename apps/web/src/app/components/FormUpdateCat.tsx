"use client"
import React, { useState, useEffect } from 'react';
import { UpdateCategoryDto } from '../../../../api/src/categories/dto/update-category.dto';
import { useSession } from 'next-auth/react';

const CategoryUpdateForm = () => {
    const [formData, setFormData] = useState<UpdateCategoryDto>({
        name: '',
    });
    const [categoryId, setCategoryId] = useState('');

    const { data: session } = useSession(); // Obtiene la sesión del usuario

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        console.log(formData);

        if (!session?.accessToken) {
            // Manejar caso donde no hay token de acceso
            console.error('No hay token de acceso disponible');
            return;
        }

        try {
            const response = await fetch(`http://localhost:4000/api/v1/categories/${categoryId}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${session.accessToken}`
                },
                body: JSON.stringify(formData),
            });

            if (response.ok) {
                console.log('Producto actualizado exitosamente');
                setFormData({
                    name: '',
                });
                setCategoryId('');
            } else {
                console.error('Error al actualizar el producto');
            }
        } catch (error) {
            console.error('Error en la solicitud:', error);
        }
    };

    useEffect(() => {
        // Aquí puedes realizar una llamada al servidor para obtener los datos del producto
        // que deseas actualizar y establecer el estado inicial de formData con esos datos
        const fetchCategoryData = async () => {
            if (!categoryId) return;

            try {
                const response = await fetch(`http://localhost:4000/api/v1/products/${categoryId}`, {
                    headers: {
                        'Authorization': `Bearer ${session?.accessToken}`
                    }
                });

                if (response.ok) {
                    const product = await response.json();
                    setFormData(product);
                } else {
                    console.error('Error al obtener los datos del producto');
                }
            } catch (error) {
                console.error('Error en la solicitud:', error);
            }
        };

        fetchCategoryData();
    }, [categoryId, session?.accessToken]);

    return (
        <div className="flex flex-col items-center justify-center bg-white p-4 rounded-lg shadow-md">
            <h2 className="text-gray-800 text-2xl font-semibold mb-4">Update Product</h2>
            <form onSubmit={handleSubmit} className="grid grid-cols-2 gap-4 w-full max-w-lg">
                <div>
                    <label htmlFor="productId" className="text-gray-700">Category ID</label>
                    <input
                        type="text"
                        name="productId"
                        placeholder="Enter product ID"
                        value={categoryId}
                        onChange={(e) => setCategoryId(e.target.value)}
                        className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
                        required
                    />
                </div>
                <div>
                    <label htmlFor="name" className="text-gray-700">Name</label>
                    <input
                        type="text"
                        name="name"
                        placeholder="Enter product name"
                        value={formData.name}
                        onChange={handleChange}
                        className="mt-1 p-2 block w-full border rounded-md bg-gray-100 text-gray-800"
                        required
                    />
                </div>
                
                <button type="submit" className="col-span-2 bg-blue-700 text-white py-2 px-4 rounded-md hover:bg-blue-600 transition-colors duration-300">Update Product</button>
            </form>
        </div>
    );
};

export default CategoryUpdateForm;