"use client";
import React from 'react'

const brandsList =[
    { brand: 'VERSAGE',},
    { brand: 'ZARA',},
    { brand: 'GUCCI',},
    { brand: 'PRADA',},
    { brand: 'Calvin Klein',},

]

const BrandsSection = () => {
  return (
    <div className='py-8 px-4 xl:gap-16 sm:py-16 xl:scroll-px-12'>
      <div className="border-black bg-black border rounded-md py-8 px-20 flex flex-row items-center justify-between">
        {brandsList.map((brand, index) => {
          return (
            <div key={index} className='flex flex-col items-center justify-center mx-4'>
              <h2 className="text-white text-3xl font-bold">{brand.brand}</h2>
            </div>
          );
        })}
      </div>
    </div> 
  );
};
export default BrandsSection
