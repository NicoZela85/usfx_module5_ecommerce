
interface ConfirmationModalProps {
  message: string;
  onConfirm: () => void;
  onCancel: () => void;
}

const ConfirmationModal: React.FC<ConfirmationModalProps> = ({ message, onConfirm, onCancel }) => {
  return (
    <div className="confirmation-modal">
      <p>{message}</p>
      <button onClick={onConfirm} className="bg-red-500 text-white px-4 py-2 mr-2">
        Confirmar
      </button>
      <button onClick={onCancel} className="bg-gray-500 text-white px-4 py-2">
        Cancelar
      </button>
    </div>
  );
};

export default ConfirmationModal;