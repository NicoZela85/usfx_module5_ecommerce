import React from "react";
import ProductCard from "./ProductCard";

interface Product {
  id: number;
  productName: string;
  price: number;
  photoUrl: string;
}

interface ProductGridProps {
  products: Product[];
}

const ProductGrid: React.FC<ProductGridProps> = ({ products }) => {
  const prefixUrl = "http://localhost:4000/";

  return (
    <div className="grid grid-cols-3 ml-40 gap-5">
      {products.map((product) => (
        <ProductCard
          key={product.id}
          productName={product.productName}
          price={product.price}
          photoUrl={prefixUrl + product.photoUrl} // Add prefix to photo URL
        />
      ))}
    </div>
  );
};

export default ProductGrid;
