import React from "react";

interface ProductCardProps {
  productName: string;
  price: number;
  photoUrl: string;
}

const ProductCard: React.FC<ProductCardProps> = ({
  productName,
  price,
  photoUrl,
}) => {
  const prefixUrl = "http://localhost:4000/";

  return (
    <div className="bg-[#CBCBCB] rounded-3xl p-2.5 mb-1 flex flex-col items-center text-black">
      <img
        src={prefixUrl + photoUrl} // Add prefix to photo URL
        alt={productName}
        className="w-full h-52 object-cover overflow-hidden m-auto rounded-3xl"
      />
      <h2 className="text-xl w-20 font-bold">{productName}</h2>

      <div className="text-xl flex items-center justify-between w-10/12">
        <button className="bg-[#ED7A56] text-black rounded-2xl p-">
          Add to cart
        </button>
        <p className="text-xs font-bold">{price.toFixed(2)} Bs.</p>
      </div>
    </div>
  );
};

export default ProductCard;
