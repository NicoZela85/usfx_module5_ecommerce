'use client'
import { useRouter } from 'next/router';
import axios from 'axios';
import { useEffect } from 'react';

interface DeleteProductComponentProps {
  productId: string; // ID del producto a eliminar
  redirectUrl: string; // URL a la que redirigir después de la eliminación
}

const DeleteProductComponent: React.FC<DeleteProductComponentProps> = ({ productId, redirectUrl }) => {
  const router = useRouter();

  useEffect(() => {
    const deleteProduct = async () => {
      try {
        const response = await axios.delete(`http://localhost:4000/api/v1/products/${productId}`);
        console.log("Producto eliminado con éxito", response.data);
        // Redirige a la URL especificada después de eliminar
        router.push(redirectUrl);
      } catch (error) {
        console.error("Error al eliminar el producto:", error);
        // Maneja el error de eliminación según sea necesario
      }
    };

    deleteProduct();
  }, [productId, redirectUrl, router]);

  return null; // No es necesario devolver nada en este componente
};

export default DeleteProductComponent;