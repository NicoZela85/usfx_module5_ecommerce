
import Link from "next/link";

const ButtonAddProduct = () => {
  return (
    <Link href="/pages/admin/createFormPro">
      <button className="px-5 py-3 text-white duration-150 bg-sky-500 rounded-full hover:bg-sky-500 active:bg-sky-700">
        + Add Product
      </button>
    </Link>

  );
}
export default ButtonAddProduct;