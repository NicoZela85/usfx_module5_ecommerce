import React from "react";
import SignOutButton from "./SignOutButton";

interface UserType {
    name?: string | null;
    email?: string | null;
    image?: string | null;
}

interface ContentSignInProps {
    user: UserType | null;
}

const ContentSignIn: React.FC<ContentSignInProps> = ({ user }) => {
    return (
        <div>
            <div className="flex flex-col items-center justify-center h-screen">
                <div className="text-center">
                    <div className="flex flex-col items-center justify-center">
                        <h1 className="text-4xl font-bold mb-8 text-center">Welcome to SupaMark!</h1>
                        <span className="text-lg mb-8">Explore products and find great deals.</span>
                        <SignOutButton />
                    </div>
                    <div className="my-8">
                        <span className="text-lg mb-8 font-bold">Your name is {user?.name}</span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContentSignIn;