'use client';
import { useState } from "react";

const SearchComp = () => {
  const [searchTerm, setSearchTerm] = useState("");

  const goToSearch = (search : string) =>  {
    console.log("Clicked on Search button");
    window.location.href = `/pages/Shop?search=${search}`;
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      console.log(searchTerm);
      goToSearch(searchTerm);
    }
  };


  return (
    <input
      type="text"
      placeholder="Search..."
      value={searchTerm}
      onChange={handleInputChange}
      onKeyPress={handleKeyPress}
      className="px-4 py-3 border border-solid border-[#999] rounded-full text-lg text-gray-700 focus:outline-none focus:border-[#555]"
      style={{ width: "50%", backgroundColor: "#F0F0F0" }} 
    />
  );
};

export default SearchComp;