import React from 'react';

const goToProductCategory = (id : number) => {
    console.log("Clicked on Shopping Cart button");
    window.location.href = `/pages/Shop?categoryId=${id}`;
};
interface Category {
    id: number;
    name: string;
    isActive: true;
    slug: string;
}

interface CategoryProps {
    category: Category;
}

const CategoryItem: React.FC<CategoryProps> = ({ category }) => {
    return (
        <div className="bg-white rounded-lg overflow-hidden shadow-lg transition duration-300 transform hover:scale-105 cursor-pointer"
            onClick={() => goToProductCategory(category.id)}>
            <div className="px-6 py-4">
                <div className="font-semibold text-lg mb-2">{category.name}</div>
                <p className="text-gray-700 text-sm">ID: {category.id}</p>
                <p className="text-gray-700 text-sm">Slug: {category.slug}</p>
                <div className="flex items-center text-sm text-gray-700">
                    Active: {category.isActive ? <span className="inline-block w-3 h-3 rounded-full bg-green-500 mr-1"></span> : <span className="inline-block w-3 h-3 rounded-full bg-red-500 mr-1"></span>} {category.isActive ? 'Yes' : 'No'}
                </div>
            </div>
        </div>
    );
};

export default CategoryItem;
