"use client";
import Share from "./Share";
const goToBack = () => {
    console.log("Clicked on Shopping Cart button");
    window.location.href = '/pages/Shop';
};
interface Product {
    id: number;
    name: string;
    description: string;
    price: number;
    stock: number;
    image: string;
    status: boolean;
    category: string;
}

interface ProductProps {
    product: Product;
}

const Description: React.FC<ProductProps> = ({ product }) => {
    const prefixUrl = "http://localhost:4000/";
    console.log(product);
    return (
        <section className='text-black'>
            <div className='container mx-auto'>
                <div className='max-w-3xl mx-auto bg-white rounded-lg shadow-lg p-8'>
                    <div className='md:flex md:justify-between md:items-center'>
                        <div className='md:w-1/2 md:mr-8'>
                            <img
                                src={prefixUrl + product.image}
                                alt={product.name}
                                width={500}
                                height={500}
                                className='rounded-md'
                            />
                        </div>
                        <div className='md:w-1/2'>
                            <h2 className='text-4xl font-bold text-black mb-8'>{product.name}</h2>
                            <div className="flex flex-col mt-4">
                                <details className="dropdown">
                                    <summary className="m-1 btn mr-3 font-semibold hover:text-black text-[#817575] border-b border-[#7E84B1]">Price</summary>
                                    <ul className="p-2 shadow menu dropdown-content z-[1] bg-base-100 rounded-box w-32">
                                        <li><a>{product.price}</a></li>
                                    </ul>
                                </details>
                                <details className="dropdown">
                                    <summary className="m-1 btn mr-3 font-semibold hover:text-black text-[#817575] border-b border-[#7E84B1]">Stock</summary>
                                    <ul className="p-2 shadow menu dropdown-content z-[1] bg-base-100 rounded-box w-32">
                                        <li><a>{product.stock}</a></li>
                                    </ul>
                                </details>
                                <details className="dropdown">
                                    <summary className="m-1 btn mr-3 font-semibold hover:text-black text-[#817575] border-b border-[#7E84B1]">Category</summary>
                                    <ul className="p-2 shadow menu dropdown-content z-[1] bg-base-100 rounded-box w-32">
                                        <li><a>{product.category}</a></li>
                                    </ul>
                                </details>
                            </div>
                            <p className='text-base lg:text-lg mt-8'>{product.description}</p>
                        </div>
                    </div>
                    <div className='flex justify-between mt-8'>
                        <button className='px-6 py-3 rounded-full bg-gradient-to-br from-black via-slate-600 to-slate-400 text-white hover:from-slate-300 hover:via-slate-400 hover:to-slate-500 transition-colors duration-300'>
                            Buy
                        </button>
                        <button
                            className='px-6 py-3 rounded-full bg-gradient-to-br from-black via-slate-600 to-slate-400 text-white hover:from-slate-300 hover:via-slate-400 hover:to-slate-500 transition-colors duration-300'
                            onClick={goToBack}>
                            Return to Previous Page
                        </button>
                    </div>
                    <Share />
                </div>
            </div>
        </section>
    );
};

export default Description;
