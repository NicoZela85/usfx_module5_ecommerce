import React from 'react';

interface Product {
    id: number;
    name: string;
    description: string;
    price: number;
    stock: number;
    image: string;
    status: boolean;
    categoryId: number;
}

interface ProductProps {
    product: Product;
}

const Product: React.FC<ProductProps> = ({ product }) => {
    const prefixUrl = "http://localhost:4000/"; // Prefix URL

    return (
        <article
            className="bg-white  p-6 mb-6 shadow transition duration-300 group transform hover:-translate-y-2 hover:shadow-2xl rounded-2xl cursor-pointer border">
            <a target="_self" href="/pages"
                className="absolute opacity-0 top-0 right-0 left-0 bottom-0"></a>
            <div className="relative mb-4 rounded-2xl">
                <img className="max-h-80 rounded-2xl w-full object-cover transition-transform duration-300 transform group-hover:scale-105"
                    src={prefixUrl + product.image} alt={product.name} /> {/* Added prefix to image URL */}
                <a className="flex justify-center items-center bg-black bg-opacity-80 z-10 absolute top-0 left-0 w-full h-full text-white rounded-2xl opacity-0 transition-all duration-300 transform group-hover:scale-105 text-xl group-hover:opacity-100"
                    href={`/pages/description?id=${product.id}`} target="_self" rel="noopener noreferrer">
                    Read description
                    <svg className="ml-2 w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M13 5l7 7-7 7M5 5l7 7-7 7"></path>
                    </svg>
                </a>
            </div>
            <div className="flex justify-between items-center w-full pb-4 mb-auto">
                <div className="flex items-center">
                    <div className="flex flex-1">
                        <div className="">
                            <p className="text-sm font-semibold ">{product.name}...</p>
                        </div>
                    </div>
                </div>
                <div className="flex justify-end">
                    <div className="text-sm flex items-center text-[#5f5f5f] ">
                        {product.price} bs.
                    </div>
                </div>
            </div>
            <h3 className="font-medium text-xl leading-8">
                <a href="/blog/slug"
                    className="block relative group-hover:text-[#838383] transition-colors duration-200 ">
                    add to shopping card
                </a>
            </h3>
            <div>
            </div>
        </article>
    );
};

export default Product;
