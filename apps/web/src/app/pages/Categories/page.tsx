"use client"
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CategoryItem from '../../components/Categorie';
import { useSession } from 'next-auth/react';

interface Category {
    id: number;
    name: string;
    isActive: true;
    slug: string;
}

const CategoriePage: React.FC = () => {
    const [categories, setCategories] = useState<Category[]>([]); 
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<string | null>(null);
    const { data: session } = useSession();

    useEffect(() => {
        const fetchCategories = async () => {
            try {
                if (!session?.accessToken) {
                    console.error('No hay token de acceso disponible');
                    return;
                }
                const response = await axios.get<Category[]>('http://localhost:4000/api/v1/categories', {
                    headers: {
                        'Authorization': `Bearer ${session.accessToken}`
                    }
                });
                console.log(response.data);
                setCategories(response.data.data);
            } catch (error) {
                setError('Error obtaining the categories');
                console.error('Error fetching categories:', error); 
            } finally {
                setIsLoading(false);
            }
        };

        fetchCategories();
    }, [session]);

    if (isLoading) {
        return <div>Cargando...</div>;
    }
    if (error) {
        return <div>{error}</div>;
    }
    if (categories == null || categories.length === 0) {
        return <div>No categories found</div>;
    }

    return (
        <div className="container mx-auto">
            <h1 className="text-3xl font-bold mb-8 text-center">Explore Our Categories</h1> 
            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-8"> 
                {categories.map(category => (
                    <CategoryItem key={category.id} category={category} />
                ))}
            </div>
        </div>
    );
};

export default CategoriePage;

