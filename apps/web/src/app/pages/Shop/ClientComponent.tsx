"use client"
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Product from '../../components/Product';
import Pagination from '../../components/Pagination';
import { useSearchParams } from 'next/navigation';
import { useSession } from 'next-auth/react';

const ClientComponent: React.FC = () => {
    const [products, setProducts] = useState<Product[]>([]);
    const [totalPages, setTotalPages] = useState<number | 0>(0);
    const { data: session } = useSession();
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<string | null>(null);

    const searchParams = useSearchParams();
    const page = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1;
    const search = searchParams.get('search') ? searchParams.get('search') : '';
    const categoryId = searchParams.get('categoryId') ? searchParams.get('categoryId') : '';


    useEffect(() => {
        const fetchProducts = async () => {
            try {
                if (!session?.accessToken) {
                    console.error('No hay token de acceso disponible');
                    return;
                }
                const response = await axios.get<Product[]>(`http://localhost:4000/api/v1/products?page=${page}&search=${search}&categoryId=${categoryId}`, {
                    headers: {
                        'Authorization': `Bearer ${session.accessToken}`
                    }
                });
                console.log(response.data);
                setTotalPages(response.data.meta.totalPages);
                setProducts(response.data.data);
            } catch (error) {
                setError('Error obtaining the products');
                console.error('Error fetching products:', error);
            } finally {
                setIsLoading(false);
            }
        };

        fetchProducts();
    }, [session]);

    if (isLoading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>{error}</div>;
    }
    if (products == null || products.length === 0) {
        return <div>products not found</div>;
    }

    return (
        <div className="container mx-auto">
            <h1 className="text-4xl font-bold text-gray-900 mb-8 text-center">Welcome to Our Product Page</h1> {/* Texto centrado y mayor espacio inferior */}
            <p className="text-lg text-gray-700 mb-8 text-center">Explore our wide range of high-quality products carefully crafted to meet your needs.</p> {/* Texto centrado */}
            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-8"> {/* Aumentamos el espacio entre las columnas */}
                {products.map(product => (
                    <Product key={product.id} product={product} />
                ))}
            </div>
            <Pagination totalPages={totalPages} currentPage={page} currentSearch={search ?? ''} categoryId={parseInt(categoryId ?? '0')} />
        </div>
    );
};

export default ClientComponent;