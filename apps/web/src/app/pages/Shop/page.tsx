import React from 'react';
const ClientComponent = React.lazy(() => import('./ClientComponent'));

const ProductsPage: React.FC = () => {
    return (
        <div className="container mx-auto">
            <React.Suspense fallback={<div>loading...</div>}>
                <ClientComponent />
            </React.Suspense>
        </div>
    );
};

export default ProductsPage;