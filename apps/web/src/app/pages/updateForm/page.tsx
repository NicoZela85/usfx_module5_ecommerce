import React from "react";
import ProductUpdateForm from '../../components/FormUpdateProduct';

export default async function Page() {
  return (
    <div className="flex justify-center items-center h-screen">
      <ProductUpdateForm/>
    </div>
  );
}
