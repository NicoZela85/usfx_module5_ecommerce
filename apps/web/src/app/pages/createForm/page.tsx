import React from "react";
import ProductCreateForm from '../../components/FormCreateProduct';

export default async function Page() {
  return (
    <div className="flex justify-center items-center h-screen">
      <ProductCreateForm/>
    </div>
  );
}
