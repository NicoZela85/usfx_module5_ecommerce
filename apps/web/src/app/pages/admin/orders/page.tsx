'use client'
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useSession } from 'next-auth/react';
import Dash from '../../../components/Dash';
interface Order {
    id: number;
    userId: string;
    address: string;
    contact: string;
    statusPayment: string;
    total: number;
    createdAt: Date;
    status: string;
}

const OrderPage: React.FC = () => {
    const [orders, setOrders] = useState<Order[]>([]); 
    const { data: session } = useSession();

    useEffect(() => {
        const fetchOrders = async () => { 
            try {
                if (!session?.accessToken) {
                    console.error('No hay token de acceso disponible');
                    return;
                }
                const response = await axios.get<{data: Order[]}>('http://localhost:4000/api/v1/order/all', {
                    headers: {
                        'Authorization': `Bearer ${session.accessToken}`
                    }
                });
                console.log(response.data);
                setOrders(response.data.data);
            } catch (error) {
                console.error('Error fetching orders:', error); 
            }
        };        
        fetchOrders();
    }, [session]);

    const handleStatusChange = async (orderId: number, newStatus: string) => {
        try {
            if (!session?.accessToken) {
                console.error('No hay token de acceso disponible');
                return;
            }
            
            await axios.put(
                `http://localhost:4000/api/v1/order/${orderId}`,
                { status: newStatus },
                {
                    headers: {
                        Authorization: `Bearer ${session.accessToken}`
                    }
                }
            );
    
            
            const updatedOrders = orders.map(order => {
                if (order.id === orderId) {
                    return { ...order, status: newStatus };
                }
                return order;
            });
            setOrders(updatedOrders);
        } catch (error) {
            console.error('Error updating order status:', error);
        }
    };
    return (
    <div className="container mx-auto px-4">
        <h1 className="text-3xl font-bold mb-4 text-black">Orders</h1> 
        <Dash />
        <div className="overflow-x-auto bg-gray-900"> 
            <table className="table-auto w-full bg-gray-900">
                <thead>
                    <tr className="bg-gray-900">
                        <th className="px-4 py-2">ID</th>
                        <th className="px-4 py-2">User ID</th>
                        <th className="px-4 py-2">Address</th>
                        <th className="px-4 py-2">Contact</th>
                        <th className="px-4 py-2">Status Payment</th>
                        <th className="px-4 py-2">Total Price</th>
                        <th className="px-4 py-2">Created At</th>
                        <th className="px-4 py-2">Status</th>
                    </tr>
                </thead>
                <tbody>
                    {orders.map(order => (
                        <tr key={order.id}>
                            <td className="border px-4 py-2 ">{order.id}</td>
                            <td className="border px-4 py-2">{order.userId}</td>
                            <td className="border px-4 py-2">{order.address}</td>
                            <td className="border px-4 py-2">{order.contact}</td>
                            <td className="border px-4 py-2">{order.statusPayment}</td>
                            <td className="border px-4 py-2">{typeof order.total === 'number' ? order.total.toFixed(2) : order.total}</td>
                            <td className="border px-4 py-2">{new Date(order.createdAt).toLocaleDateString()}</td>
                            <td className="border px-4 py-2">
                                <select value={order.status} onChange={(e) => handleStatusChange(order.id, e.target.value)} className="bg-gray-800 text-white px-2 py-1 rounded">
                                    <option value="PENDING">PENDING</option>
                                    <option value="COMPLETED">COMPLETED</option>
                                </select>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    </div>
    );
};

export default OrderPage;
