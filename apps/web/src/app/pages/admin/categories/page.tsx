'use client';

import ButtonAdd from "../../../components/ButtonAdd";
import Dash from "../../../components/Dash";
import TableDataCategories from "../../../components/TableDataCategories";


export default async function Page() {
  return (
    <>

      <div className="flex flex-wrap">
        <div className="w-1/5">
          <Dash />
        </div>
        <div className="ml-64 mr-0 overflow-y-scroll h-screen ">
          <p className="text-3xl mb-8 mt-8 font-bold">Categories</p>
          <div className="rounded-lg bg-white shadow-md p-4 flex items-center justify-between mb-15">
            <div className="flex items-center justify-center">
              <p className="text-lg font-semibold mr-12">List of Categories</p>
              <ButtonAdd />
            </div>
          </div>
          <main className="flex-grow flex justify-center items-center">
            <div className="overflow-x-auto w-full h-full">
              <TableDataCategories />
            </div>
          </main>


        </div>
      </div>
    </>


  );
}





