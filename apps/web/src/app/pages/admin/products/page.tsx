'use client';

import Dash from "../../../components/Dash";
import ButtonAddProduct from "../../../components/ProductsAdmin/ButtonAddProduct";
import TableDataProducts from "../../../components/ProductsAdmin/TableDataProducts";


export default async function Pageaea() {
  return (
    <>

      <div className="flex flex-wrap">
        <div className="w-1/5">
          <Dash />
        </div>
        <div className="ml-64 mr-0 overflow-y-scroll h-screen ">
          <p className="text-3xl mb-8 mt-8 font-bold">Products</p>
          <div className="rounded-lg bg-white shadow-md p-4 flex items-center justify-between mb-15">
            <div className="flex items-center justify-center">
              <p className="text-lg font-semibold mr-12">List of Products</p>
                <ButtonAddProduct/>
            </div>
          </div>
          <main className="flex-grow flex justify-center items-center">
            <div className="overflow-x-auto w-full h-full">
              <TableDataProducts/>
            </div>
          </main>


        </div>
      </div>
    </>


  );
}