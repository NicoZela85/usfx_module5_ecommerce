import React from "react";
import Dash from "../../../components/Dash";
import ProductUpdateForm from "../../../components/FormUpdateProduct";


export default async function Page() {
  return (
    <>
      <Dash />
        <div className="flex justify-center items-center h-screen">
          <ProductUpdateForm />
        </div>
    </>
  );
}