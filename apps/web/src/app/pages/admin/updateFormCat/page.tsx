import React from "react";
import Dash from "../../../components/Dash";
import CategoryUpdateForm from "../../../components/FormUpdateCat";


export default async function Page() {
  return (
    <><Dash /><div className="flex justify-center items-center h-screen">
      <CategoryUpdateForm />
    </div></>
  );
}