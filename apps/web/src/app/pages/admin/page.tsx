"use client";
import Dash from "../../components/Dash";
import Header from "../../components/Header";
import HeaderDash from "../../components/HeaderDash";
import Carrito from "../../images/carrito2.png";



export default function Page() {
  return (
    <div className="flex flex-wrap">
      <div className="w-1/5">
        <Dash />
      </div>
      <div className="ml-64 mr-0 bg-slate-100 overflow-y-scroll h-screen antialiased text-slate-300 selection:bg-blue-600 selection:text-white ">
        <div className="flex-1 flex flex-col justify-center items-center p-8">
        <HeaderDash />
        
          <h1 className="text-3xl font-bold text-gray-800 mb-4">¡Bienvenido al Dashboard de Administración del Supermercado!</h1>
          <div className="mb-6">
            <img src={Carrito.src} alt="Supermercado" className="w-full rounded-lg shadow-lg" />
          </div>
          <div className="mb-8">
            <h2 className="text-2xl font-semibold text-gray-800 mb-2">Misión:</h2>
            <p className="text-lg text-gray-600">Nuestra misión es proporcionar a nuestra comunidad productos frescos y de alta calidad, a precios accesibles, con un servicio al cliente excepcional, creando así una experiencia de compra única y satisfactoria.</p>
          </div>
          <div>
            <h2 className="text-2xl font-semibold text-gray-800 mb-2">Visión:</h2>
            <p className="text-lg text-gray-600">Nuestra visión es ser el supermercado preferido de nuestra comunidad, reconocido por nuestro compromiso con la calidad, la excelencia en el servicio y la responsabilidad social, contribuyendo así al bienestar y desarrollo de nuestro entorno.</p>
          </div>
        </div>
      </div>
    </div>
  );
}
