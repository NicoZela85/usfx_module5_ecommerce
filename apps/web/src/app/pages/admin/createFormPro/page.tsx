'use client';
import React from "react";
import ProductCreateForm from "../../../components/FormCreateProduct";
import Dash from "../../../components/Dash";


export default async function CreateFormProduct() {
  return (
    <>
      <Dash />
      <div className="ml-64 mr-0 overflow-y-scroll h-screen antialiased text-slate-300 selection:bg-blue-600 selection:text-white mt-8 pt-10">
        <p className="text-3xl text-black mb-8 mt-8 font-bold">Products</p>
        <ProductCreateForm />
      </div>
    </>
  );
}