
import React from "react";
import CategoryCreateForm from "../../../components/FormCreateCat";
import Dash from "../../../components/Dash";


export default async function Page() {
  return (
    <>
      <Dash />
      <div className="ml-64 mr-0 overflow-y-scroll h-screen antialiased text-slate-300 selection:bg-blue-600 selection:text-white mt-8 pt-10">
        <p className="text-3xl text-black mb-8 mt-8 font-bold">Categories</p>
        <CategoryCreateForm />
      </div>
    </>
  );
}
