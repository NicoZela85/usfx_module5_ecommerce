"use client";
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useSession } from 'next-auth/react';
import { useSearchParams } from 'next/navigation';
import Description from '../../components/Description';

interface Product {
    id: number;
    name: string;
    description: string;
    price: number;
    stock: number;
    image: string;
    status: boolean;
    category: string;
}

const ClientComponent: React.FC = () => {
    const [product, setProduct] = useState<Product | null>(null);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<string | null>(null);
    const { data: session } = useSession();
    const searchParams = useSearchParams();
    const id = searchParams.get('id');
    console.log(id);

    useEffect(() => {
        const fetchProduct = async () => {
            setIsLoading(true);
            setError(null);

            try {
                if (!session?.accessToken) {
                    setError('No hay token de acceso disponible');
                    return;
                }

                const response = await axios.get<Product>(`http://localhost:4000/api/v1/products/${id}`, {
                    headers: {
                        'Authorization': `Bearer ${session.accessToken}`
                    }
                });
                setProduct(response.data.data.attributes);
            } catch (error) {
                setError('Error obtaining the description');
                console.error('Error fetching product:', error);
            } finally {
                setIsLoading(false);
            }
        };

        if (id) {
            fetchProduct();
        }
    }, [id, session]);

    if (isLoading) {
        return <div>Cargando...</div>;
    }

    if (error) {
        return <div>{error}</div>;
    }

    if (!product) {
        return <div>Description not found</div>;
    }

    return <Description product={product} />;
};

export default ClientComponent;