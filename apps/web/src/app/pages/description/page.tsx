import React from 'react';
const ClientComponent = React.lazy(() => import('./ClientComponent'));

const DescriptionPage: React.FC = () => {
    return (
        <div className="container mx-auto">
            <React.Suspense fallback={<div>Cargando...</div>}>
                <ClientComponent />
            </React.Suspense>
        </div>
    );
};

export default DescriptionPage;