import React from "react";
import { getServerSession } from 'next-auth'
import { authOptions } from '../app/api/auth/[...nextauth]/route'
import SignInButton from "../app/components/SignInButton"
import Navbar from "../app/components/NavBarComp"
import Footer from "../app/components/Footer"
import BrandsSection from "../app/components/BrandsSection"
import Welcome from "../app/components/Welcome"


export default async function Page() {
  return (
    <div>
      <div className="container mx-auto px-12 py-4">
        <Welcome />
      </div>
      <BrandsSection />
    </div>
  );
}
