declare namespace NodeJS {
  export interface ProcessEnv {
    NEXTAUTH_URL: string
    NEXTAUTH_SECRET: string
    FRONTEND_CLIENT_ID: string
    FRONTEND_CLIENT_SECRET: string
    AUTH_ISSUER: string
  }
}